# zblog

#### 介绍
thymeleaf
layui
shiro   角色授权    方法授权
springboot
mybatisplus
百度富文本编辑器
redis
redis+shiro=session共享
实现简单的SSO单点登录

#### 软件架构
软件架构说明


#### 安装教程

1.  jdk1.8
2.  作者本人使用mysql8.0.16如果需要更换，请更改pom.xml文件和application.properties两处
3.  redis 更换 redis密码 ，作者使用默认端口，如果不是默认端口自行更改
4.  请自行配置短信注册!
     1、注册个163邮箱  ，然后看下放图片说明，得到自己的  SMTP服务器  地址  然后到application.properties中找到spring.mail.host赋值给他
     2、把账号赋值给spring.mail.username     把密码赋值给spring.mail.password
![输入图片说明](https://images.gitee.com/uploads/images/2019/1030/204943_34346e09_4866948.png "QQ截图20191030204828.png")
5.  使用的时候数据库链接？后面请自行更改（不更改可能会出现地区问题）
#### 使用说明

1.  端口号在application.properties中自行看（开发中，可能随时会变），访问直接输入 http://localhost:端口号   即可访问到网站首页，http://localhost:端口号/admin   访问到登录页面
2.  
3.  xxxx

#### 参与贡献

1.  
2.  
3. 
4.  


#### 码云特技

1. 
2.  
3. 
4.  
5. 
6. 
