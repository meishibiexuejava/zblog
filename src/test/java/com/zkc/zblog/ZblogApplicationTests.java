package com.zkc.zblog;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zkc.zblog.entity.ZblogCategory;
import com.zkc.zblog.entity.ZblogUser;
import com.zkc.zblog.mapper.ZblogCategorymapper;
import com.zkc.zblog.mapper.ZblogUsermapper;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import javax.annotation.Resources;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ZblogApplicationTests {

    @Resource
    ZblogUsermapper zblogUsermapper;


    @Resource
    ZblogCategorymapper zblogCategorymapper;

    @Test
    public void contextLoads() {


        Page<ZblogCategory> page=new Page<>(1,20);
        QueryWrapper<ZblogCategory> queryWrapper=new QueryWrapper();
        queryWrapper.orderByAsc("fid");
        Page<ZblogCategory> Pages=zblogCategorymapper.selectAllList(page,queryWrapper);


        System.out.println(Pages);


    }

}






















