/*
 Navicat Premium Data Transfer

 Source Server         : 8.0.16
 Source Server Type    : MySQL
 Source Server Version : 80016
 Source Host           : localhost:3306
 Source Schema         : zblog

 Target Server Type    : MySQL
 Target Server Version : 80016
 File Encoding         : 65001

 Date: 05/08/2020 16:13:23
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for qrtz_blob_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `BLOB_DATA` blob NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_blob_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_calendars
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `CALENDAR_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `CALENDAR` blob NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `CALENDAR_NAME`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_calendars
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_cron_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `CRON_EXPRESSION` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TIME_ZONE_ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_cron_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_fired_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ENTRY_ID` varchar(95) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `INSTANCE_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `FIRED_TIME` bigint(13) NOT NULL,
  `SCHED_TIME` bigint(13) NOT NULL,
  `PRIORITY` int(11) NOT NULL,
  `STATE` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `JOB_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `IS_NONCONCURRENT` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `REQUESTS_RECOVERY` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`, `ENTRY_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_fired_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_job_details
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `DESCRIPTION` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `JOB_CLASS_NAME` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `IS_DURABLE` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `IS_NONCONCURRENT` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `IS_UPDATE_DATA` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `REQUESTS_RECOVERY` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_DATA` blob NULL,
  PRIMARY KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_job_details
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_locks
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `LOCK_NAME` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `LOCK_NAME`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_locks
-- ----------------------------
INSERT INTO `qrtz_locks` VALUES ('RuoyiScheduler', 'STATE_ACCESS');
INSERT INTO `qrtz_locks` VALUES ('RuoyiScheduler', 'TRIGGER_ACCESS');

-- ----------------------------
-- Table structure for qrtz_paused_trigger_grps
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_GROUP`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_paused_trigger_grps
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_scheduler_state
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `INSTANCE_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `LAST_CHECKIN_TIME` bigint(13) NOT NULL,
  `CHECKIN_INTERVAL` bigint(13) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `INSTANCE_NAME`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_scheduler_state
-- ----------------------------
INSERT INTO `qrtz_scheduler_state` VALUES ('RuoyiScheduler', 'WIN-030964RVLJ71580647162526', 1580647614560, 15000);

-- ----------------------------
-- Table structure for qrtz_simple_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `REPEAT_COUNT` bigint(7) NOT NULL,
  `REPEAT_INTERVAL` bigint(12) NOT NULL,
  `TIMES_TRIGGERED` bigint(10) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_simple_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_simprop_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `STR_PROP_1` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `STR_PROP_2` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `STR_PROP_3` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `INT_PROP_1` int(11) NULL DEFAULT NULL,
  `INT_PROP_2` int(11) NULL DEFAULT NULL,
  `LONG_PROP_1` bigint(20) NULL DEFAULT NULL,
  `LONG_PROP_2` bigint(20) NULL DEFAULT NULL,
  `DEC_PROP_1` decimal(13, 4) NULL DEFAULT NULL,
  `DEC_PROP_2` decimal(13, 4) NULL DEFAULT NULL,
  `BOOL_PROP_1` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `BOOL_PROP_2` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_simprop_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `DESCRIPTION` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `NEXT_FIRE_TIME` bigint(13) NULL DEFAULT NULL,
  `PREV_FIRE_TIME` bigint(13) NULL DEFAULT NULL,
  `PRIORITY` int(11) NULL DEFAULT NULL,
  `TRIGGER_STATE` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_TYPE` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `START_TIME` bigint(13) NOT NULL,
  `END_TIME` bigint(13) NULL DEFAULT NULL,
  `CALENDAR_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `MISFIRE_INSTR` smallint(2) NULL DEFAULT NULL,
  `JOB_DATA` blob NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  INDEX `SCHED_NAME`(`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) USING BTREE,
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) REFERENCES `qrtz_job_details` (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for zblog_article
-- ----------------------------
DROP TABLE IF EXISTS `zblog_article`;
CREATE TABLE `zblog_article`  (
  `wid` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '文章表主键',
  `articlename` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '文章名字',
  `articlekeyword` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文章关键字',
  `articledescription` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文章描述',
  `artuclecontent` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '文章内容',
  `artuclethumbnail` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文章缩略图',
  `artuledata` datetime(0) NOT NULL COMMENT '文章创建时间',
  `artuleuser` bigint(255) NULL DEFAULT NULL COMMENT '文章列表关联的用户表 （zblog_user）外键',
  `artulestatus` bigint(255) NOT NULL COMMENT '1  需审核      2直接通过',
  `artulecategory` bigint(255) NOT NULL COMMENT '文章类别          关联主键(zblog_category)',
  `artulecarousel` bigint(255) NOT NULL COMMENT '0 不是轮播图推荐文章  1  是轮播图推荐文章',
  PRIMARY KEY (`wid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 27 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of zblog_article
-- ----------------------------
INSERT INTO `zblog_article` VALUES (1, '天下第一文章', '天下第一文章', '春秋时期，老子 [1]  集古圣先贤之大智慧。总结了古老的道家思想的精华，形成了道家完整系统的理论，标志着道家思想已经正式成型。道家是对中华哲学、文学、科技、艺术、音乐、养生、宗教等等影响最深远的学派。\n李约瑟说：“中国人性格中有许多最吸引人的因素都来源于道家思想。中国如果没有道家思想，就像是一棵深根已经烂掉的大树。”“道家思想乃是中国的科学和技术的根本”，', '<p style=\"text-indent: 2em; text-align: left;\"><img src=\"http://img.baidu.com/hi/jx2/j_0016.gif\"/>18:49:13春秋时期，老子 [1] &nbsp;集古圣先贤之大智慧。总结了古老的道家思想的精华，形成了道家完整系统的理论，标志着道家思想已经正式成型。道家是对中华哲学、文学、科技、艺术、音乐、养生、宗教等等影响最深远的学派。\n李约瑟说：“中国人性格中有许多最吸引人的因素都来源于道家思想。中国如果没有道家思想，就像是一棵深根已经烂掉的大树。”“道家思想乃是中国的科学和技术的根本”，“(道家哲学)对中国科学史是有着头等重要性的”，对整个东亚和东南亚的宗教、思想、文化都产生了较大影响。\n有中华文化基石之称的道家哲学思想，还通过儒学以及中国化之后的佛学而得以不同程度的体现。\n道家以“道”为核心，认为大道无为、主张道法自然，提出道生法、以雌守雄、刚柔并济等政治、经济、治国、军事策略，具有朴素的辩证法思想，是“诸子百家”中一门极为重要的哲学流派，存在于中华各文化领域，对中国乃至世界的文化都产生了巨大的影响。大量的中外学者开始注意到与吸取道家的积极思想，故学者说：“道家思想可以看为中国民族伟大的产物。是国民思想的中心，大有‘仁者见之谓之仁，知者见之谓之知，百姓日用而不知’的气概。”\n东汉末年道教以黄、老道家思想为理论根据，承袭春秋战国以来的神仙方术衍化形成。</p>', '/zblogimg/2020/04/13/7d48e3e118fafc169bdab7a6a4d60390.png', '2019-09-01 12:15:14', 20190823195722, 2, 2, 0);
INSERT INTO `zblog_article` VALUES (25, '鞠婧祎的腰围让人羡慕，被威亚吊成90度，这才是真正的海绵腰', '鞠婧祎', '近日，鞠婧祎和宋威龙主演的《美丽书生》大热，原以为是一出雷剧，但由于剧组人员的用心，以及主演的倾情演绎，所以收效依然不错，鞠婧祎在剧中扮演了一位穷书生，穿着白大褂来到云上学堂读书，或许很多人都认为鞠婧祎长得那么漂亮，演男装怎么能有说服力呢？甚至于正也对该剧还是近视眼矫正中心一说不屑一顾，但鞠婧祎在剧中的人设看起来就像个女孩子的“男子汉”，所以清秀娇小一点也没', '<p style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; line-height: 24px; color: rgb(51, 51, 51); text-align: left; font-family: arial; white-space: normal; background-color: rgb(255, 255, 255); text-indent: 2em;\">近日，鞠婧祎和宋威龙主演的《美丽书生》大热，原以为是一出雷剧，但由于剧组人员的用心，以及主演的倾情演绎，所以收效依然不错，鞠婧祎在剧中扮演了一位穷书生，穿着白大褂来到云上学堂读书，或许很多人都认为鞠婧祎长得那么漂亮，演男装怎么能有说服力呢？甚至于正也对该剧还是近视眼矫正中心一说不屑一顾，但鞠婧祎在剧中的人设看起来就像个女孩子的“男子汉”，所以清秀娇小一点也没什么关系。</p><p style=\"text-align:center\"><img src=\"https://pics4.baidu.com/feed/d058ccbf6c81800ac1292e6c875638fd838b476a.jpeg?token=7a3c18b1f0fc1a9b73698feb9a2d518c\"/></p><p style=\"margin-top: 26px; margin-bottom: 0px; padding: 0px; line-height: 24px; color: rgb(51, 51, 51); text-align: justify; font-family: arial; white-space: normal; background-color: rgb(255, 255, 255); text-indent: 2em;\"><span class=\"bjh-p\">尽管鞠婧祎在剧中多男装，但也有女装，比如有个穿红衣服的古装造型，而且鞠婧祎和宋威龙虽然是第一次合作，但意外地有了 cp感，剧组曝出两人合作的花絮，其中有一段是鞠婧祎“掉下来”，宋威龙接住了她的戏，男才女貌，甚至连绿幕前的合作都相当养眼。</span></p><p style=\"text-align: center; text-indent: 2em;\"><img class=\"large\" src=\"https://pics2.baidu.com/feed/562c11dfa9ec8a13c2552830c3609a88a1ecc079.jpeg?token=010cdc618b709b3b22c415ad0b058324\"/></p><p style=\"margin-top: 26px; margin-bottom: 0px; padding: 0px; line-height: 24px; color: rgb(51, 51, 51); text-align: justify; font-family: arial; white-space: normal; background-color: rgb(255, 255, 255); text-indent: 2em;\"><span class=\"bjh-p\">没有几个明星喜欢吊威亚，虽然拍出来的效果看起来仙气十足，但毕竟太考验身体的柔韧度了，时间久了身体就会很不舒服，但是鞠婧祎意外，难怪大家都叫她鞠婧祎古装女神，她古装吊威亚的动作真的太好看了，虽然当了演员，但鞠婧祎的舞蹈功底还在，在被威亚吊起做“下坠”的动作时，身体直接呈现“直角”，这一柔韧度也太绝了吧。</span></p><p style=\"text-align: center; text-indent: 2em;\"><img class=\"large\" src=\"https://pics5.baidu.com/feed/342ac65c103853430cf09961a670bb79ca808819.jpeg?token=aae28aa568c8edd5dd7c596a66ce8b94\"/></p><p style=\"text-align: center; text-indent: 2em;\"><img class=\"large\" src=\"https://pics4.baidu.com/feed/962bd40735fae6cd7f8a01ed3ad0042343a70fed.jpeg?token=a5b777860d9cdb643fcf8af87744379c\"/></p><p style=\"margin-top: 26px; margin-bottom: 0px; padding: 0px; line-height: 24px; color: rgb(51, 51, 51); text-align: justify; font-family: arial; white-space: normal; background-color: rgb(255, 255, 255); text-indent: 2em;\"><span class=\"bjh-p\">前一阵子孟美岐的“海绵腰”上了热搜，但鞠婧祎一点都不差，看鞠婧祎吊起威亚的下腰的幅度，完全符合传说中的“海绵腰”，而且鞠婧祎不需要员工的帮助，可以自己一个人完成，要知道在空中只有几根细线，是很难保持平衡的，因为鞠婧祎吊起的好，接人的宋威龙也显得轻松许多。真的要鞠躬尽瘁，死而后已，再拍几部仙侠剧，再拍几个飞行镜头，一定非常漂亮，有一种“海绵腰”叫鞠婧祎，被威亚吊成90度，全网都看傻了，你怎么看？</span></p>', '/zblogimg/2020/07/29/d41cb59bc56481d3393f4deac7f603b2.jpeg', '2020-07-29 17:50:58', 20190828183000, 2, 1, 0);
INSERT INTO `zblog_article` VALUES (26, '成团后共度的首个生日！硬糖少女为希林娜依高庆生', '林娜依高生日', '7月31日是硬糖少女303成员希林娜依高的生日，零点刚到，硬糖少女303官微就在微博晒出成员们为她庆生的合照。随后，陈卓璇、刘些宁、张艺凡也在微博发文为她送上祝福。', '<p style=\"margin-top: 10px; margin-bottom: 20px; padding: 0px; list-style: none; line-height: 30px; word-break: break-all; text-align: center; text-indent: 2em;\"><img alt=\"\" src=\"https://m1-1253159997.image.myqcloud.com/imageDir/3678bb77b81423e23a00889474760cc5.jpg\"/></p><p style=\"margin-top: 10px; margin-bottom: 20px; padding: 0px; list-style: none; line-height: 30px; word-break: break-all; text-align: left; text-indent: 2em;\">7月31日是硬糖少女303成员希林娜依高的生日，零点刚到，硬糖少女303官微就在微博晒出成员们为她庆生的合照。随后，陈卓璇、刘些宁、张艺凡也在微博发文为她送上祝福。</p><p style=\"margin-top: 10px; margin-bottom: 20px; padding: 0px; list-style: none; line-height: 30px; word-break: break-all; text-align: center; text-indent: 2em;\"><img alt=\"\" src=\"https://m1-1253159997.image.myqcloud.com/imageDir/19b2d504fa17544629312f6310bffd34.png\"/></p><p style=\"margin-top: 10px; margin-bottom: 20px; padding: 0px; list-style: none; line-height: 30px; word-break: break-all; text-align: center; text-indent: 2em;\"><img alt=\"\" src=\"https://m1-1253159997.image.myqcloud.com/imageDir/10ac9b377ef35fed8f2a5fa023047fa0.png\"/></p><p style=\"margin-top: 10px; margin-bottom: 20px; padding: 0px; list-style: none; line-height: 30px; word-break: break-all; text-align: center; text-indent: 2em;\"><img alt=\"\" src=\"https://m1-1253159997.image.myqcloud.com/imageDir/691efdefdc3ee870d481523388f6bc27.png\"/></p>', '/zblogimg/2020/08/03/6a63e10a34391fb4102e2f13f5930ff1.jpg', '2020-08-03 18:53:07', 20190828183000, 2, 2, 0);

-- ----------------------------
-- Table structure for zblog_category
-- ----------------------------
DROP TABLE IF EXISTS `zblog_category`;
CREATE TABLE `zblog_category`  (
  `fid` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '文章分类id',
  `category` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '文章类别',
  PRIMARY KEY (`fid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of zblog_category
-- ----------------------------
INSERT INTO `zblog_category` VALUES (1, '游戏');
INSERT INTO `zblog_category` VALUES (2, '娱乐');
INSERT INTO `zblog_category` VALUES (3, '历史');
INSERT INTO `zblog_category` VALUES (20, '科技');

-- ----------------------------
-- Table structure for zblog_left_nav
-- ----------------------------
DROP TABLE IF EXISTS `zblog_left_nav`;
CREATE TABLE `zblog_left_nav`  (
  `nid` bigint(50) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `code` bigint(100) NOT NULL COMMENT '编号  （随机编号测试数据因是手动添加所以就和id一样，后期是随机的UUID）',
  `codeid` bigint(100) NOT NULL COMMENT '父编号',
  `nname` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单名称',
  `icon` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图标',
  `level` bigint(10) NOT NULL COMMENT '菜单等级',
  `url` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '跳转地址 / 目录',
  `sort` bigint(255) NULL DEFAULT NULL COMMENT '排序字段（越小越靠前）',
  PRIMARY KEY (`nid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of zblog_left_nav
-- ----------------------------
INSERT INTO `zblog_left_nav` VALUES (1, 1, 0, '主页', 'layui-icon-home', 1, '目录', 1);
INSERT INTO `zblog_left_nav` VALUES (2, 2, 0, '内容管理', 'layui-icon-flag', 1, '目录', 2);
INSERT INTO `zblog_left_nav` VALUES (3, 3, 0, '系统设置', 'layui-icon-set', 1, '目录', 3);
INSERT INTO `zblog_left_nav` VALUES (4, 4, 0, '用户管理', 'layui-icon-user', 1, '目录', 4);
INSERT INTO `zblog_left_nav` VALUES (5, 5, 0, '退出', 'layui-icon-logout', 1, '/nav/logout', 5);
INSERT INTO `zblog_left_nav` VALUES (6, 6, 1, '控制台', 'layui-icon-console', 2, '/nav/default', 8);
INSERT INTO `zblog_left_nav` VALUES (7, 7, 1, '主页一', 'layui-icon-flag', 2, '', 6);
INSERT INTO `zblog_left_nav` VALUES (8, 8, 1, '主页二', 'layui-icon-flag', 2, '', 7);
INSERT INTO `zblog_left_nav` VALUES (9, 9, 2, '文章管理', 'layui-icon-list', 2, '目录', 9);
INSERT INTO `zblog_left_nav` VALUES (10, 10, 2, '分类管理', 'layui-icon-flag', 2, '/nav/Category', 10);
INSERT INTO `zblog_left_nav` VALUES (11, 11, 3, '网站设置', 'layui-icon-flag', 2, '', 11);
INSERT INTO `zblog_left_nav` VALUES (12, 12, 4, '后台管理员', 'layui-icon-flag', 2, '/nav/user', 12);
INSERT INTO `zblog_left_nav` VALUES (13, 13, 9, '文章列表', 'layui-icon-flag', 3, '/nav/article', 13);
INSERT INTO `zblog_left_nav` VALUES (14, 14, 9, '定时文章', 'layui-icon-flag', 3, '/nav/timed_article', 14);
INSERT INTO `zblog_left_nav` VALUES (15, 15, 3, '系统日志', 'layui-icon-flag', 2, '', 15);
INSERT INTO `zblog_left_nav` VALUES (16, 16, 3, '菜单管理', 'layui-icon-flag', 2, '/nav/menu', 16);

-- ----------------------------
-- Table structure for zblog_user
-- ----------------------------
DROP TABLE IF EXISTS `zblog_user`;
CREATE TABLE `zblog_user`  (
  `uid` bigint(255) NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `uname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名',
  `ucode` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户账号',
  `upassword` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '密码',
  `usex` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '性别 1保密  2男  3女',
  `uavatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像',
  `usignature` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '个性签名',
  `ustatus` bigint(255) NOT NULL COMMENT '激活状态    1未激活   2已激活',
  `uclassification` bigint(20) NOT NULL COMMENT '用户分类    1超级管理员   2管理员',
  `yids` bigint(20) NOT NULL COMMENT '用户外键',
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '邮箱',
  `userdata` datetime(0) NOT NULL COMMENT '用户注册时间',
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of zblog_user
-- ----------------------------
INSERT INTO `zblog_user` VALUES (1, '卡拉肖克*玲', '2123470297@qq.com', '0ddcf32c7fa9047d9fd8ab76efd398e7', '3', '/zblogimg/2020/04/12/2f4186802499765df7d02416cc20c228.png', '他会循着冲天朔发出的召唤,就像听着来自远天的战歌,一边随着轻声吟唱,一边走向他的战器,冲天朔,没有人能够阻挡他,因为他是世间最伟大的魁拔。', 2, 1, 20190823195722, '2123470297@qq.com', '2019-08-24 08:12:15');
INSERT INTO `zblog_user` VALUES (2, '卡拉肖克*雄', '2418870649@qq.com', '0ddcf32c7fa9047d9fd8ab76efd398e7', '2', '/zblogimg/2020/04/12/e89706538e33088c1b9267558c1b2900.png', '浮云骑士', 2, 2, 20190828183000, '2418870649@qq.com', '2019-08-28 08:00:00');
INSERT INTO `zblog_user` VALUES (3, '卡拉肖克*潘', '2519274708@qq.com', '0ddcf32c7fa9047d9fd8ab76efd398e7', '2', '/zblogimg/2020/04/12/cf55ec74897a2b2d896145e303f87293.png', '暴力不解决问题', 2, 2, 20190828200522, '2509274708@qq.com', '2019-08-28 08:00:00');

SET FOREIGN_KEY_CHECKS = 1;
