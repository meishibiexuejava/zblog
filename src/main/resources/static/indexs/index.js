layui.use(['element', 'layer', 'jquery', 'laytpl', 'colorpicker'], function () {
    var element = layui.element;
    var jquery = layui.$;
    var layer = layui.layer;
    var laytpl = layui.laytpl;
    var colorpicker = layui.colorpicker;


    jquery(function () {
        jquery.ajax({
            url: "/zblogLeftNav/tree"
            , type: "get"
            , success: function (restlt) {
                var left_nav = ""
                jquery.each(restlt.data, function (i, item) {
                    left_nav += indicato.first(item);
                })
                jquery(".layui-side-menu .layui-side-scroll").append('<ul class="navBar layui-nav layui-nav-tree" lay-shrink="all">' + left_nav + '</ul>')
                element.init()
            }
        })
    })
    /**
     *菜单渲染方法
     */
    var indicato = {
        /**
         * 一级菜单渲染
         * */
        first: function (menu) {
            if (menu.children.length === 0) {
                return laytpl('<li class="layui-nav-item"><a href="{{d.href}}"><i class="layui-icon {{d.one_icon}}"></i><cite>{{d.title}}</cite></a></li>').render(menu)
            } else {
                var first = '<a href="javascript:;"><i class="layui-icon {{d.one_icon}}"></i> <cite>{{d.title}}</cite><span class="layui-nav-more"></span></a>'
                var generator = '';
                if (menu.children != null) {
                    jquery.each(menu.children, function (i, item) {
                        generator += indicato.generator(item);
                    })
                }
                return laytpl('<li class="layui-nav-item">' + first + '<dl class="layui-nav-child">' + generator + '</dl> </li>').render(menu)
            }
        },
        /**
         * 二级菜单渲染
         * */
        generator: function (menu) {
            if (menu.children.length === 0) {
                var generator = '<dd><a href="javascript:;" data-id="{{d.id}}" data-title="{{d.title}}" data-url="{{d.href}}" class="site-demo-active"><i class="layui-icon {{d.two_icon}}"></i> <cite>{{d.title}}</cite></a></dd>';
            } else {
                var stowasser = "";
                if (menu.children != null) {
                    jquery.each(menu.children, function (i, item) {
                        stowasser += indicato.stowasser(item);
                    })
                }
                generator = '<dd><a href="javascript:;"><i class="layui-icon {{d.two_icon}}"></i> <cite>{{d.title}}</cite><span class="layui-nav-more"></span></a> <dl class="layui-nav-child">' + stowasser + '</dl> </dd>'
            }
            return laytpl(generator).render(menu);
        },
        /**
         * 三级菜单渲染
         * */
        stowasser: function (menu) {
            var stowasser = '<dd><a href="javascript:;" data-id="{{d.id}}" data-title="{{d.title}}" data-url="{{d.href}}" class="site-demo-active"><i class="layui-icon {{d.three_icon}}"></i> <cite>{{d.title}}</cite></a></dd>';
            return laytpl(stowasser).render(menu);
        }
    }
    /**
     * 左侧导航栏菜单
     * */
    jquery('body').on('click', '.site-demo-active', function () {
        var dataid = jquery(this);
        //这时会判断右侧.layui-tab-title属性下的有lay-id属性的li的数目，即已经打开的tab项数目
        if (jquery(".layui-tab-title li[lay-id]").length <= 0) { //判断选项卡的个数是否为0  如若等于0直接打开新的选项卡
            active.tabAdd(dataid.attr("data-url"), dataid.attr("data-id"), dataid.attr("data-title"));
        } else {
            //否则判断该tab项是否以及存在
            var isData = false; //初始化一个标志，为false说明未打开该tab项 为true则说明已有
            jquery.each(jquery(".layui-tab-title li[lay-id]"), function () {
                //如果点击左侧菜单栏所传入的id 在右侧tab项中的lay-id属性可以找到，则说明该tab项已经打开
                if (jquery(this).attr("lay-id") == dataid.attr("data-id")) {
                    isData = true;
                }
            })
            if (isData == false) {
                //标志为false 新增一个tab项
                active.tabAdd(dataid.attr("data-url"), dataid.attr("data-id"), dataid.attr("data-title"));
            }
        }
        //最后不管是否新增tab，最后都转到要打开的选项页面上
        active.tabChange(dataid.attr("data-id"));
        rollPosition()
    });
    /**
     * （JavaScript 对象方法）
     * */
    var active = {
        tabAdd: function (url, id, name) {
            /**
             * 新增一个Tab项 传入三个参数
             * 1、tab页面的地址  是标签中的 data-url 的属性值
             * 2、一个规定的id，是标签中data-id的属性值
             * 3、标题   是标签中的 data-title 的属性值
             * */
            element.tabAdd('demo', {    //这里的demo 对应 tab元素的 lay-filter="demo" 过滤器的值（demo）
                title: '<span>' + name + '</span>',
                content: '<iframe data-frameid="' + id + '" scrolling="auto" frameborder="0" src="' + url + '" style="width:100%;height:100%;"></iframe>',
                id: id //规定好的id
            })
        },
        /**
         * 选项卡切换
         * */
        tabChange: function (id) {
            element.tabChange('demo', id); //根据传入的id传入到指定的tab项
        },
        /**
         * 选项卡删除
         * */
        tabDelete: function (id) {
            element.tabDelete("demo", id);
        }
    };
    /**
     * 选项卡左右操作
     * */
    jquery(".layui-tab-roll-left").click(function () {
        rollClick("left");
    });
    jquery(".layui-tab-roll-right").click(function () {
        rollClick("right");
    });
    /**
     * 点选项卡左右事件
     * @param direction
     */
    function rollClick(direction) {
        var tabTitle = jquery('.layui-tab .layui-tab-title');
        var left = tabTitle.scrollLeft();
        if ('left' === direction) {
            tabTitle.animate({
                scrollLeft: left - 450
            }, 200);
        } else {
            tabTitle.animate({
                scrollLeft: left + 450
            }, 200);
        }
    }
    /**
     * 自动定位
     */
    function rollPosition() {
        var tabTitle = jquery('.layui-tab  .layui-tab-title');
        var autoLeft = 0;
        tabTitle.children("li").each(function () {
            if (jquery(this).hasClass('layui-this')) {
                return false;
            } else {
                autoLeft += jquery(this).outerWidth();
            }
        });
        tabTitle.animate({
            scrollLeft: autoLeft - tabTitle.width() / 3
        }, 200);
    }
    /**
     * 选项卡   触碰显示的三个按钮的操作
     */
    jquery('body').on('click', '[layui-tab-close]', function () {
        var loading = layer.load(0, {shade: false, time: 2 * 1000});
        var closeType = jquery(this).attr('layui-tab-close');
        jquery(".layui-tab .layui-tab-title li").each(function () {
            var tabId = jquery(this).attr('lay-id');
            var id = jquery(this).attr('id');
            var isCurrent = jquery(this).hasClass('layui-this');
            if (id !== 'top_tabs') {
                if (closeType === 'all') {
                    if (tabId != 6)
                        active.tabDelete(tabId)
                } else {
                    if (closeType === 'current' && isCurrent) {
                        if (tabId != 6)
                            active.tabDelete(tabId)
                    } else if (closeType === 'other' && !isCurrent) {
                        if (tabId != 6)
                            active.tabDelete(tabId)
                    }
                }
            }
        });
        layer.close(loading);
    });
    /**
     * 隐藏左侧导航
     * */
    jquery('body').on('click', '[LAY_app_flexible]', function () {
        jquery(".layui-layout-admin").toggleClass("showMenu");
        var nav = jquery(this).attr('LAY_app_flexible');
        if (nav == 'put') {
            jquery(this).attr('LAY_app_flexible', 'close');
            jquery(this).html('<i class="layui-icon layui-icon-spread-left"></i>');
        } else {
            jquery(this).attr('LAY_app_flexible', 'put');
            jquery(this).html('<i class="layui-icon layui-icon-shrink-right"></i>');
        }
    });
    /**
     * 刷新当前
     *
     * 此处添加禁止连续点击刷新一是为了降低服务器压力，
     * 另外一个就是为了防止超快点击造成chrome本身的一些js文件的报错(不过貌似这个问题还是存在，不过概率小了很多)
     * */
    jquery(".refresh").on("click", function () {
        if (jquery(this).hasClass("refreshThis")) {
            jquery(this).removeClass("refreshThis");
            jquery(".layui-tab-content .layui-tab-item.layui-show").find("iframe")[0].contentWindow.location.reload();
            setTimeout(function () {
                jquery(".refresh").addClass("refreshThis");
            }, 2000)
        } else {
            layer.msg("您点击的速度超过了服务器的响应速度，还是等两秒再刷新吧！");
        }
    })
    /**
     * 全屏操作
     */
    jquery('body').on('click', '[data-check-screen]', function () {
        var check = jquery(this).attr('data-check-screen');
        if (check == 'full') {
            zblogAdmin.fullScreen();
            jquery(this).attr('data-check-screen', 'exit');
            jquery(this).html('<i class="layui-icon layui-icon-screen-restore"></i>');
        } else {
            zblogAdmin.exitFullScreen();
            jquery(this).attr('data-check-screen', 'full');
            jquery(this).html('<i class="layui-icon layui-icon-screen-full"></i>');
        }
    });
    /**
     * 全屏此操作事件
     * */
    var zblogAdmin = {
        /**
         * 进入全屏
         */
        fullScreen: function () {
            var el = document.documentElement;
            var rfs = el.requestFullScreen || el.webkitRequestFullScreen;
            if (typeof rfs != "undefined" && rfs) {
                rfs.call(el);
            } else if (typeof window.ActiveXObject != "undefined") {
                var wscript = new ActiveXObject("WScript.Shell");
                if (wscript != null) {
                    wscript.SendKeys("{F11}");
                }
            } else if (el.msRequestFullscreen) {
                el.msRequestFullscreen();
            } else if (el.oRequestFullscreen) {
                el.oRequestFullscreen();
            } else {
                zblogAdmin.error('浏览器不支持全屏调用！');
            }
        },
        /**
         * 退出全屏
         */
        exitFullScreen: function () {
            var el = document;
            var cfs = el.cancelFullScreen || el.webkitCancelFullScreen || el.exitFullScreen;
            if (typeof cfs != "undefined" && cfs) {
                cfs.call(el);
            } else if (typeof window.ActiveXObject != "undefined") {
                var wscript = new ActiveXObject("WScript.Shell");
                if (wscript != null) {
                    wscript.SendKeys("{F11}");
                }
            } else if (el.msExitFullscreen) {
                el.msExitFullscreen();
            } else if (el.oRequestFullscreen) {
                el.oCancelFullScreen();
            } else {
                zblogAdmin.error('浏览器不支持全屏调用！');
            }
        },
        error: function (title) {
            return layer.msg(title, {icon: 2, shade: this.shade, scrollbar: false, time: 3000, shadeClose: true});
        }
    }







});
