layui.use(['form', 'layer', 'jquery','upload','laydate','table'], function () {
    var form = layui.form, jquery = layui.$, layer = layui.layer, upload = layui.upload,laydate =layui.laydate,table=layui.teable;
    /**
    时间选择器
    */
    laydate.render({
        elem: '#datacontent'
        ,type: 'datetime'
    });

    //实例化编辑器
    //建议使用工厂方法getEditor创建和引用编辑器实例，如果在某个闭包下引用该编辑器，直接调用UE.getEditor('editor')就能拿到相关的实例
    var ue = UE.getEditor('editor');

    UE.Editor.prototype._bkGetActionUrl = UE.Editor.prototype.getActionUrl;
    UE.Editor.prototype.getActionUrl = function(action) {
        if (action == 'uploadimage' || action == 'uploadscrawl' || action == 'uploadimage') {
            return '/config2';
        } else if (action == 'uploadvideo') {

        } else {
            return this._bkGetActionUrl.call(this, action);
        }
    };

    /**
     * 百度富文本编辑器失去焦点后  获取180个字 为描述
     * */
    ue.addListener("blur",function(){
        var value=jquery("#articledescription").val();
        var description= UE.getEditor('editor').getContentTxt();
        if(value==''||value==null){
            jquery("#articledescription").val(description.toString().substring(0,180));//从内容中最多获取180字，设置成描述
        }
    });
    /**
     *描述框失去焦点
     * */
    jquery("#articledescription").blur(function(){
        var value=jquery("#articledescription").val();
        var description= UE.getEditor('editor').getContentTxt();
        if(value==''||value==null){
            jquery("#articledescription").val(description.toString().substring(0,180));//从内容中最多获取180字，设置成描述
        }
    });



    //进入页面直接修改checkbox无法在off状态下获取值
    //是否审核
    jquery("#sh").val(2).attr('type', 'hidden');
    //是否推向轮播
    jquery("#lb").val(0).attr('type', 'hidden');

    /*激活*/
    form.on('switch(sh)', function (data) {   //lay-filter="sh"
        jquery(data.elem).attr('type', 'hidden').val(this.checked ? 1 : 2);
    });
    /*轮播图*/
    form.on('switch(lb)', function (data) {   //lay-filter="lb"
        jquery(data.elem).attr('type', 'hidden').val(this.checked ? 1: 0);
    });

    /*单张图片上传*/
    upload.render({
        elem:"#scimg"   //上传按钮的id
        ,url:"/upload"  //上传接口
        ,method:"post"  //请求方式
        ,size: "1024" //上传大小
        ,accept:"images" //上传类型
        ,daag: "true"  //允许拖动上传
        ,auto: true //允许自动上传
        ,field: "imgFile" //整个图片的数据存放在这个属性中 要与后台接收图片的属性名字保持一致
        ,before:function (obj) {
            //弹出的loading
            var indexs=top.layer.msg('图片上传中，请稍候',{icon: 16, time: false, shade: 0.8})
        }
        ,done:function (res,index,upload) {//res（服务端响应信息）、index（当前文件的索引）、upload（重新上传的方法，一般在文件上传失败后使用）
            if(res.data!="修改头像失败！"){
                //上传完毕回调
                jquery(".imgss img").remove()
                jquery(".imgss input").remove()
                jquery(".imgss").append("<img style='width: 100px;height: 100px;'  src='" + res.data + "'/>")
                    .append("<input name=\"artuclethumbnail\" value='" + res.data + "' type='text' style=\"display: none\">")
                layer.close(index);//关闭loading
                top.layer.msg("图片上传成功！");
            }else{
                layer.close(index);//关闭loading
                top.layer.msg("上传失败！");
            }
        }
        ,error:function (index,upload) {
            layer.close(index)
        }

    })


    /*
    修改数据
    */
    form.on("submit(addNew)",function (data) {

        var formDatas = data.field;//获取form数
        //弹出loading
        var index = top.layer.msg('数据提交中，请稍候', {icon: 16, time: false, shade: 0.8});
        jquery.ajax({
            url: "/zblogArticle/updateArtic"
            , contentType: "application/json; charset=utf-8"
            , type: "put"
            , datType: "josn"
            , async: false
            , data: JSON.stringify(formDatas)
            , success: function (result) {
                layer.close(index)
                top.layer.msg(result.msg);
                parent.location.reload();//刷新数据并关闭添加页面
            }
        });
    })













});