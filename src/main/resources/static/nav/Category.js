layui.use(['table', 'jquery', 'layer'], function () {
    var table = layui.table;
    var jquery = layui.$;
    var layer = layui.layer;


    table.render({
        elem: '#CategoryAll'   //table表格id
        , url: '/zblogCategory/zblogCategoryList'   //访问路径
        , request: {     //修改layui的默认参数名字
            pageName: 'current'   //当前页   默认 page
            , limitName: 'size'  //页大小   默认limit
        }
        , title: '分类列表'
        , toolbar: 'default'  //开启工具栏，此处显示默认图标 ,也可以自定义模板，详细看文档
        , page: true  //开启自动分页
        , limit: 25   //设置页大小
        , limits: [25, 50, 100, 200, 400]  //设置一页分别可以有多少数据
        , parseData: function (res) { //res 即为原始返回的数据    //如果不是layui想要的json格式可进行数据解析成固定的格式    //简单点说就是数据指向对应的名字
            return {
                "code": res.code, //解析接口状态
                "msg": res.msg, //解析提示文本
                "count": res.data.total, //解析数据长度
                "data": res.data.records //解析数据列表
            };
        }
        , cols: [[     //表头
            {type: 'checkbox', fixed: 'left'}
            , {field: 'fid', title: '分类ID', sort: true}
            , {field: 'category', title: '分类名字', sort: true}
            , {fixed: 'right', title: '操作', toolbar: '#CategoryDemo'}
        ]]
    })

    /**
     * 头工具事件
     */
    table.on("toolbar(CategoryAll)",function (obj) {    //这里的ArticleAll对应这 table中的lay-filter的值
        var layEvent=obj.event;  //获取lay-event里面的值
        var checkStatus = table.checkStatus('CategoryAll')
             , data = checkStatus.data;   // 获取数据

        if(layEvent === "add"){  //头工具事件添加
            layer.open({
                type: 2
                , title: "分类添加"  //弹出框名称
                , area: ['35%', '25%']     //弹出框大小  宽  ， 高
                , shift: 5
                , maxmin: true
                , offset: 'auto'
                , content: "/nav/category/categorymodeladd" //访问路径
            })
        }else if(layEvent ==="update"){ //头工具事件修改
            if(data.length===1){
                layer.open({
                    type: 2
                    , title: "分类修改"  //弹出框名称
                    , area: ['35%', '25%']     //弹出框大小  宽  ， 高
                    , shift: 5
                    , maxmin: true
                    , offset: 'auto'
                    , content: "/nav/category/categorymodelupdate/"+data[0].fid //访问路径
                })
            }else{
                layer.msg("修改只能修改一个！")
            }
        }else if(layEvent === "delete"){ //头工具事件 多选删除

            if(data.length===0){
                layer.msg("请选择要删除的分类！");
            }else{

                var arr = [];
                for (var i = 0; i < data.length; i++) {    //循环筛选出id
                    arr.push(data[i].fid);
                }

                layer.confirm('真的要删除这'+data.length+'分类吗？',function () {
                    jquery.ajax({
                        url:"/zblogCategory/zblogcategorydeletelist"
                        ,type: "post"
                        ,dataType:"json"
                        , data: JSON.stringify(arr)
                        ,contentType:"application/json"
                        ,success:function (restlt) {
                            layer.closeAll();//关闭弹窗
                            layer.msg(restlt.msg);
                            location.reload();//刷新数据并关闭添加页面
                        }
                    })
                })

            }

        }
    });


    /**
     * 行工具事件
     */
    table.on("tool(CategoryAll)", function(obj) {
        var data=obj.data;  //获取这一行的数据
        var layEvent=obj.event;  //获取lay-event的值
        if(layEvent ==="edit"){        //行工具事件修改
            layer.open({
                type: 2
                , title: "分类修改"  //弹出框名称
                , area: ['35%', '25%']     //弹出框大小  宽  ， 高
                , shift: 5
                , maxmin: true
                , offset: 'auto'
                , content: "/nav/category/categorymodelupdate/"+data.fid //访问路径
            })
        }else if(layEvent === "del"){    //行工具事件删除
            layer.confirm('真的要删除分类吗？',function () {
                jquery.ajax({
                    url:"/zblogCategory/zblogcategorydelete/"+data.fid
                    ,type: "delete"
                    ,success:function (restlt) {
                        layer.closeAll();//关闭弹窗
                        obj.del();//删除对应行（tr）的DOM结构
                        layer.msg(restlt.msg);
                    }
                })
            })
        }
    })













});