layui.use(['table', 'jquery', 'layer'], function () {
    var table = layui.table;
    var jquery = layui.$;
    var layer = layui.layer;


    table.render({
        elem: '#ArticleAll'   //table表格id
        , url: '/zblogArticle/zblogArticleList'   //访问路径
        , request: {     //修改layui的默认参数名字
            pageName: 'current'   //当前页   默认 page
            , limitName: 'size'  //页大小   默认limit
        }
        , title: '文章列表'
        , toolbar: 'default'  //开启工具栏，此处显示默认图标 ,也可以自定义模板，详细看文档
        , page: true  //开启自动分页
        , limit: 25   //设置页大小
        , limits: [25, 50, 100, 200, 400]  //设置一页分别可以有多少数据
        , parseData: function (res) { //res 即为原始返回的数据    //如果不是layui想要的json格式可进行数据解析成固定的格式    //简单点说就是数据指向对应的名字
            return {
                "code": res.code, //解析接口状态
                "msg": res.msg, //解析提示文本
                "count": res.data.total, //解析数据长度
                "data": res.data.records //解析数据列表
            };
        }
        , cols: [[     //表头
            {type: 'checkbox', fixed: 'left'}
            , {field: 'wid', title: 'ID', unresize: true, sort: true}
            , {
                field: 'zblogUser', title: '作者', sort: true ,templet:function (d) {
                    return d.zblogUser.uname
                }
              }
            , {field: 'articlename', title: '文章标题', sort: true}
            , { 
                field: 'artuclethumbnail', title: '缩略图', sort: true,templet:function (d) {
                    if(d.artuclethumbnail ===null || d.artuclethumbnail === ""){
                        return "<a></a>";
                    }else{
                        return "<img src='" + d.artuclethumbnail + "' style='width: 40px;height: 30px'/>";
                    }
                }
              }
            , {
                field: 'zblogCategory', title: '所属分类', sort: true ,templet:function (d) {
                    return d.zblogCategory.category
                }
              }
            , {field: 'artuledata', title: '发布时间', sort: true ,templet:"<div>{{layui.util.toDateString(d.artuledata, 'yyyy年MM月dd日 HH点mm分ss秒')}}</div>"}
            , {fixed: 'right', title: '操作', toolbar: '#ArticleDemo'}
        ]]
    })



    //头工具事件
    table.on("toolbar(ArticleAll)",function (obj) {    //这里的ArticleAll对应这 table中的lay-filter的值
        var layEvent=obj.event;  //获取lay-event里面的值
        var checkStatus = table.checkStatus("ArticleAll");
        var data = checkStatus.data;


        if(layEvent === "add"){   //头工具事件添加
            layer.open({//进入管理员添加页面
                type: 2
                , title: "管理员添加页面"  //弹出框名称
                , area: ['90%', '80%']     //弹出框大小
                , shift: 5
                , maxmin: true
                , offset: 'auto'
                , content: "/nav/article/articleadd" //访问路径
            })
        }else if(layEvent ==="update"){  //头工具事件修改
            if(data.length===1){
                layer.open({//进入文章修改页面
                    type: 2
                    , title: "文章修改页面"  //弹出框名称
                    , area: ['90%', '80%']     //弹出框大小
                    , shift: 5
                    , maxmin: true
                    , offset: 'auto'
                    , content: "/nav/article/articleupdate/"+data[0].wid //访问路径
                })
            }else{
                layer.msg("请选择一个经行修改！")
            }

        }else if(layEvent === "delete"){  //头工具事件多选删除
            var arr = [];
            for (var i = 0; i < data.length; i++) {    //循环筛选出id
                arr.push(data[i].wid);
            }
            layer.confirm('真的要删除这'+data.length+'篇文章吗？',function () {
                jquery.ajax({
                    url:"/zblogArticle/DeleteArticList"
                    ,type: "post"
                    ,dataType:"json"
                    , data: JSON.stringify(arr)
                    ,contentType:"application/json"
                    ,success:function (restlt) {
                        layer.closeAll();//关闭弹窗
                        layer.msg(restlt.msg);
                        location.reload();//刷新数据并关闭添加页面
                    }
                })
            })
        }

    });






    //行工具事件
    table.on("tool(ArticleAll)", function(obj) {
        var data=obj.data;  //获取这一行的数据
        var layEvent=obj.event;  //获取lay-event的值
        if(layEvent ==="edit"){      //行工具事件修改
            layer.open({//进入文章修改页面
                type: 2
                , title: "文章修改页面"  //弹出框名称
                , area: ['90%', '80%']     //弹出框大小
                , shift: 5
                , maxmin: true
                , offset: 'auto'
                , content: "/nav/article/articleupdate/"+data.wid //访问路径
            })
        }else if(layEvent === "del"){  //行工具事件删除
            layer.confirm('真的要删除这篇文章吗？',function () {
                 jquery.ajax({
                     url:"/zblogArticle/deleteArtic/"+data.wid
                     ,type: "delete"
                     ,success:function (restlt) {
                         layer.closeAll();//关闭弹窗
                         obj.del();//删除对应行（tr）的DOM结构
                         layer.msg(restlt.msg);
                     }
                 })
            })
        }else if(layEvent === "chalook"){  //行工具事件查看
            layer.open({//进入文章查看页面
                type: 2
                , title: "文章查看页面"  //弹出框名称
                , area: ['90%', '80%']     //弹出框大小
                , shift: 5
                , maxmin: true
                , offset: 'auto'
                , content: "/nav/article/articlelook/"+data.wid //访问路径
            })
        }
    })






});