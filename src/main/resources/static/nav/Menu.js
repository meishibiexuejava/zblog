layui.use(['element', 'jquery', 'layer','laytpl','tree','util'], function () {
    var element= layui.element;
    var jquery = layui.$;
    var layer = layui.layer;
    var laytpl = layui.laytpl;
    var tree = layui.tree;
    var util = layui.util;



    jquery(function () {
        jquery.ajax({
            url:"/zblogLeftNav/tree"
            ,type: "get"
            ,success:function (restlt) {
                //无连接线风格
                tree.render({
                    elem: '#demo_tree'
                    ,data: restlt.data
                    ,onlyIconControl: true  //否仅允许节点左侧图标控制展开收缩。
                    ,isJump : false   //是否允许点击节点时弹出新窗口跳转。
                    ,showLine : true  //是否开启连接线
                    ,accordion : true  //是否开启手风琴模式
                    ,edit: ['add', 'update', 'del'] //是否开启节点的操作图标
                    ,click: function(obj){
                        //console.log(obj.data); //得到当前点击的节点数据
                        // console.log(obj.state); //得到当前节点的展开状态：open、close、normal
                        // console.log(obj.elem); //得到当前节点元素
                        // console.log(obj.data.children); //当前节点下是否有子节点
                        // layer.msg(JSON.stringify(obj.data));   // JSON.stringify()值转换为 JSON 字符串。
                        layer.open({  //修改
                            type: 2
                            , title: "编辑页面"  //弹出框名称
                            , area: ['100%', '100%']     //弹出框大小
                            , shift: 5
                            , maxmin: true
                            , offset: 'auto'
                            , content: "/nav/nav_menu/menu_update/"+obj.data.id //访问路径
                        })
                    }
                });
            }
        })
    })
















});
