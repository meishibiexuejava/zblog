layui.use(['layer','jquery'], function () {
    var jquery = layui.$, layer = layui.layer;
    /**
     * 查看公告信息
     **/
    jquery('body').on('click', '.layuimini-notice', function () {
        var title = jquery(this).children('.layuimini-notice-title').text();      /* 标题 */
        var noticeTime = jquery(this).children('.layuimini-notice-extra').text(); /* 时间 */
        var content = jquery(this).children('.layuimini-notice-content').html();  /* 内容 */

        var html = '<div style="padding:15px 20px; text-align:justify; line-height: 22px;border-bottom:1px solid #e2e2e2;background-color: #2f4056;color: #ffffff">\n' +
            '<div style="text-align: center;margin-bottom: 20px;font-weight: bold;border-bottom:1px solid #718fb5;padding-bottom: 5px"><h4 class="text-danger">' + title + '</h4></div>\n' +
            '<div style="font-size: 12px">' + content + '</div>\n' +
            '</div>\n';                                                            /* 内容样式 */


        parent.layer.open({   /* 加上 parent 可以全局遮罩,为什么这样不清楚,以后再了解 */
            type: 1,
            title: [noticeTime,'font-size: 12px;color: #b1b3b9;margin-top: 1px; text-align: center;'],
            area: '300px;',
            shade: 0.8,
            id: 'layuimini-notice',  /*不知道什么效果，因该是可有可无*/
            btn: ['查看', '取消'],
            btnAlign: 'c',
            moveType: 1,/*按钮排列   btnAlign: 'l' 按钮左对齐 ，btnAlign: 'c' 按钮居中对齐 ，btnAlign: 'r' 按钮右对齐。默认值，不用设置*/
            content:html,
            success: function (result) {
                var btn = result.find('.layui-layer-btn');/*find() 方法返回被选元素的后代元素。后代是子、孙、曾孙，依此类推*/
                btn.find('.layui-layer-btn0').attr({href: 'http://127.0.0.1', target: '_blank'});
            }
        });
    });








});