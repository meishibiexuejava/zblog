layui.use(['table', 'jquery', 'layer'], function () {
    var table = layui.table;
    var jquery = layui.$;
    var layer = layui.layer;


    table.render({
        elem: '#UserAll'   //table表格id
        , url: '/zblogUser/UserAll'   //访问路径
        , request: {     //修改layui的默认参数名字
            pageName: 'current'   //当前页   默认 page
            , limitName: 'size'  //页大小   默认limit
        }
        , title: '用户数据表'
        , toolbar: 'default'  //开启工具栏，此处显示默认图标 ,也可以自定义模板，详细看文档
        , page: true  //开启自动分页
        , limit: 25   //设置页大小
        , limits: [25, 30, 40, 50, 60, 70, 80, 90, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 2000]  //设置一页分别可以有多少数据
        , parseData: function (res) { //res 即为原始返回的数据    //如果不是layui想要的json格式可进行数据解析成固定的格式    //简单点说就是数据指向对应的名字
            return {
                "code": res.code, //解析接口状态
                "msg": res.msg, //解析提示文本
                "count": res.data.total, //解析数据长度
                "data": res.data.records //解析数据列表
            };
        }
        , cols: [[     //表头
            {type: 'checkbox', fixed: 'left'}
            , {field: 'uid', title: 'ID', unresize: true, sort: true}
            , {field: 'uname', title: '用户名', sort: true}
            , {field: 'ucode', title: '账号', width: 170, sort: true}
            , {field: 'upassword', title: '密码', width: 270, sort: true}
            , {
                field: 'usex', title: '性别', width: 80, sort: true ,templet:function (d) {
                    if(d.usex==1){
                        return "<a>保密</a>";
                    }else if(d.usex==2){
                        return "<a>男</a>";
                    }else if(d.usex==3){
                        return "<a>女</a>";
                    }
                }
              }
            , {
                 field: 'uavatar', title: '头像', sort: true, width: 70,templet:function (d) {
                       if(d.uavatar ===null || d.uavatar === ""){
                           return "<a></a>";
                       }else{
                           return "<img class='layui-anim-rotate' src='" + d.uavatar + "' style='width: 40px;height: 30px'/>";
                       }
                 }
              }
            , {field: 'usignature', title: '个性签名', sort: true}
            , {
                 field: 'ustatus', title: '激活状态', width: 100, sort: true ,templet :function (d) {
                       if(d.ustatus==1){
                           return "<a>未激活</a>";
                       }else{
                           return "<a>已激活</a>";
                       }
                 }
              }
            , {
                 field: 'uclassification', title: '用户分类', width: 100, sort: true ,templet :function(d) {
                      if(d.uclassification==1){
                          return "<a>超级管理员</a>";
                      }else{
                          return "<a>管理员</a>";
                      }
                 }
              }
            , {field: 'email', title: '注册邮箱', sort: true ,width: 180}
            , {
                 field: 'userdata', title: '注册时间', width: 240, sort: true ,templet:"<div>{{layui.util.toDateString(d.userdata, 'yyyy年MM月dd日 HH点mm分ss秒')}}</div>"
              }
            , {fixed: 'right', width: 120, title: '操作', toolbar: '#barDemo'}
        ]]
    })

    /**
     *头工具事件
     *
     * */
    table.on("toolbar(UserAll)",function (obj) {    //这里的UserAll对应这 table中的lay-filter的值
        var layEvent=obj.event;
        var checkStatus = table.checkStatus('UserAll')
            , data = checkStatus.data;   // 获取数据

        if(layEvent === "add"){   //头工具事件添加
            layer.open({//进入管理员添加页面
                type: 2
                , title: "管理员添加页面"  //弹出框名称
                , area: ['90%', '80%']     //弹出框大小
                , shift: 5
                , maxmin: true
                , offset: 'auto'
                , content: "/nav/user/UserAlladdye" //访问路径
            })
        }else if(layEvent ==="update"){ //头工具事件修改
            if(data.length===1){
                layer.open({//进入修改页面
                    type: 2
                    , title: "管理员编辑页面"  //弹出框名称
                    , area: ['90%', '80%']     //弹出框大小
                    , shift: 5
                    , maxmin: true
                    , offset: 'auto'
                    , content: "/nav/user/UserAllUpdate/" + data[0].uid   //访问路径
                })
            }else{
                layer.msg("修改只能修改一个！")
            }
        }else if(layEvent === "delete"){ //  头工具事件多选删除

            var arr = [];
            for (var i = 0; i < data.length; i++) {    //循环筛选出id
                arr.push(data[i].uid);
            }

            layer.confirm('真的要删除这'+data.length+'个用户吗？',function () {
                jquery.ajax({
                    url:"/zblogUser/UserDeleteList"
                    ,type: "post"
                    ,dataType:"json"
                    , data: JSON.stringify(arr)
                    ,contentType:"application/json"
                    ,success:function (restlt) {
                        layer.closeAll();//关闭弹窗
                        layer.msg(restlt.msg);
                        location.reload();//刷新数据并关闭添加页面
                    }
                })
            })



        }

    });

    /**
     * 行工具事件
     */
    table.on("tool(UserAll)", function(obj) { //注：tool 是工具条事件名，UserAll 是 table 原始容器的属性 lay-filter="对应的值"
        var data=obj.data; //当前数据
        var layEvent=obj.event;  //当前的行工具事件
        if(layEvent ==="edit"){   // 行工具事件 修改
            layer.open({//进入修改页面
                type: 2
                , title: "管理员编辑页面"  //弹出框名称
                , area: ['90%', '80%']     //弹出框大小
                , shift: 5
                , maxmin: true
                , offset: 'auto'
                , content: "/nav/user/UserAllUpdate/" + data.uid   //访问路径
            })
        }else if(layEvent === "del"){   //行工具事件删除
            layer.confirm('真的删除行么', function () {
                jquery.ajax({
                    url: "/zblogUser/UserDelete/" + data.uid
                    , type: "delete"
                    , success: function (restlt) {
                        layer.closeAll();
                        obj.del(); //删除对应行（tr）的DOM结构
                        layer.msg(restlt.msg);
                    }
                })
            });
        }
    })

});
