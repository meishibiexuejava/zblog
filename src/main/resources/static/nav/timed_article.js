layui.use(['table', 'jquery', 'layer'], function () {
    var table = layui.table;
    var jquery = layui.$;
    var layer = layui.layer;

    table.render({
        elem: '#timed_articlle'   //table表格id
        , url: '/timing/joblist'   //访问路径
        , request: {     //修改layui的默认参数名字
            pageName: 'current'   //当前页   默认 page
            , limitName: 'size'  //页大小   默认limit
        }
        , title: '用户数据表'
        , toolbar: 'default'  //开启工具栏，此处显示默认图标 ,也可以自定义模板，详细看文档
        , page: true  //开启自动分页
        , limit: 25   //设置页大小
        , limits: [25, 30, 40, 50, 60, 70, 80, 90, 100]  //设置一页分别可以有多少数据
        , parseData: function (res) { //res 即为原始返回的数据    //如果不是layui想要的json格式可进行数据解析成固定的格式    //简单点说就是数据指向对应的名字
            return {
                "code": res.code, //解析接口状态
                "msg": res.msg, //解析提示文本
                "count": res.data.total, //解析数据长度
                "data": res.data.records //解析数据列表
            };
        }
        ,cols:[[
            {type: 'checkbox', fixed: 'left'}
            , {field: 'jobName', title: '定时任务名称', sort: true}  //sort是否排序
            , {field: 'jobGroup', title: '定时任务组', sort: true}
            , {field: 'jobClassName', title: '定时任务全类名', sort: true}
            , {field: 'triggerName', title: '触发器名称', sort: true}
            , {field: 'triggerGroup', title: '触发器组', sort: true}
            , {field: 'repeatInterval', title: '重复间隔', sort: true}
            , {field: 'timesTriggered', title: '触发次数', sort: true}
            , {field: 'cronExpression', title: 'cron表达式', sort: true}
            , {field: 'timeZoneId', title: '时区', sort: true}
            , {field: 'triggerState', title: '定时任务状态', sort: true}
            , {fixed: 'right', title: '操作', toolbar: '#timed_barDemo'}  //行工具栏id
        ]]
    })

    /**
     * 行工具监听事件
     * */
    table.on('tool(timed_articlle)',function (obj) {  //注：tool 是工具条事件名，timed_articlle 是 table 原始容器的属性 lay-filter="对应的值"
        var data = obj.data; //获得当前行数据
        var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
        var tr = obj.tr; //获得当前行 tr 的 DOM 对象（如果有的话）


        switch(layEvent) {
            case "del":
                layer.confirm('真的删除行么', function(index){
                    jquery.ajax({
                        url:"/timing/deleteJob"
                        , type: "delete"
                        , contentType: "application/json; charset=utf-8"
                        , datType: "josn"
                        , data:JSON.stringify({
                            "jobClassName":data.jobName,
                            "jobGroupName":data.jobGroup
                        })
                        , success: function(result){
                            obj.del(); //删除对应行（tr）的DOM结构，并更新缓存
                            layer.closeAll();
                            layer.msg(result.msg)
                        }
                    });
                });
                break;
            case "edit":
                layer.msg("行工具编辑事件")
                break;

        }

    })


    /**
    * 头工具栏事件
    * */
    table.on('toolbar(timed_articlle)', function(obj){

        switch(obj.event){
            case "add":
                layer.open({//添加定时任务
                    type: 2
                    , title: "添加定时任务页面"  //弹出框名称
                    , area: ['90%', '80%']     //弹出框大小
                    , shift: 5
                    , maxmin: true
                    , offset: 'auto'
                    , content: "/nav/quartz/addjob" //访问路径
                })
                break;
            case "update":
                layer.msg("编辑");
                break;
            case "delete":
                layer.msg("多行删除或者单行删除");
                break;
        }



    });






});