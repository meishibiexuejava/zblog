layui.use(['form', 'layer', 'jquery','upload','laydate'], function () {

    var form = layui.form, jquery = layui.$, layer = layui.layer, upload = layui.upload,laydate =layui.laydate;


    /*
    修改的表单提交
    */
    form.on("submit(addNews)", function (data) {
        var formData = data.field;//获取form数据

        //弹出loading
        var index = top.layer.msg('数据提交中，请稍候', {icon: 16, time: false, shade: 0.8});
        jquery.ajax({
            url: "/zblogUser/UserAllUpdate"
            , contentType: "application/json; charset=utf-8"
            , type: "put"
            , datType: "josn"
            , async: false
            , data: JSON.stringify(formData)
            , success: function (result) {
                    layer.close(index);
                    top.layer.msg(result.msg);
                    parent.location.reload();//刷新数据并关闭修改页面
            }
        });
    });



    /*激活*/
    form.on('switch(jh)', function (data) {   //lay-filter="jh"
        jquery(data.elem).attr('type', 'hidden').val(this.checked ? 2 : 1);
    });


    /*
    时间选择器
    */
    laydate.render({
        elem: '#test1'
        ,type: 'datetime'
    });


    /*
    *单个图片上传
    */
    upload.render({
        elem: "#img"  //上传按钮的id
        ,url: "/upload"   //上传接口
        ,method: "post"  //请求凡事
        ,size: 10000  //最大允许上传文件的大小
        ,accept: "images"  //上传文件的类型
        ,daag: true  //允许拖拽
        ,auto: true  //允许自动上传
        ,field: "imgFile"   // 整个图片的数据存放在这个属性中 要与后台接收图片的属性名字保持一致
        ,before: function (obj) {
            //弹出loading
            var index = top.layer.msg('数据提交中，请稍候', {icon: 16, time: false, shade: 0.8});
        }
        ,done: function(res,index,upload) {//res（服务端响应信息）、index（当前文件的索引）、upload（重新上传的方法，一般在文件上传失败后使用）
            if(res.data!="修改头像失败！"){
                //上传完毕回调
                jquery(".imgs img").remove()
                jquery(".imgs input").remove()
                jquery(".imgs").append("<img style='width: 100px;height: 100px;'  src='" + res.data + "'/>").append("<input name=\"uavatar\" value='" + res.data + "' type='text' style=\"display: none\">")
                layer.close(index);//关闭loading
                top.layer.msg("图片上传成功！");
            }else{
                layer.close(index);//关闭loading
                top.layer.msg("上传失败！");
            }
        }
        ,error:function (index,upload) {
            layer.close(index);//关闭loading
            top.layer.msg("上传失败！");
        }
    })





});