layui.use(['form', 'layer', 'jquery','upload','laydate'], function () {

    var form = layui.form,
        jquery = layui.$,
        layer = layui.layer,
        upload = layui.upload;

    /**
     * 表单提交
     * */
    form.on("submit(Menu)", function (data) {
        var formData = data.field;//获取form数据
        console.log(formData)
        //弹出loading
        var index = top.layer.msg('数据提交中，请稍候', {icon: 16, time: false, shade: 0.8});
        jquery.ajax({
            url: "/zblogLeftNav/updatemenu"
            , contentType: "application/json; charset=utf-8"
            , type: "put"
            , datType: "josn"
            , async: false
            , data: JSON.stringify(formData)
            , success: function (result) {
                layer.close(index)
                top.layer.msg(result.msg);
                parent.location.reload();//刷新数据并关闭添加页面
            }
        });
    });

    /**
     * 图标弹出框
     * */
    jquery("#icon").on("click",function () {
        layer.open({
            type: 2 //此处以iframe举例
            ,title: '图标'
            ,area: ['100%', '100%']
            ,shade: 0
            ,maxmin: true
            // ,offset: [ //为了演示，随机坐标
            //     Math.random()*(jquery(window).height()-300)
            //     ,Math.random()*(jquery(window).width()-390)
            // ]
            ,content: '/nav/nav_menu/icon'
            ,btn: ['关闭'] //只是为了演示
            ,yes: function(){
                layer.closeAll();
            }
            ,zIndex: layer.zIndex //重点1
            ,success: function(layero){
                parent.layer.setTop(layero); //重点2
            }
        });
    })






});
