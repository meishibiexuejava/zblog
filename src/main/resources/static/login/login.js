layui.use(['form', 'layer', 'jquery', 'layedit', 'laydate'], function () {
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : parent.layer,
        laypage = layui.laypage,
        layedit = layui.layedit,
        laydate = layui.laydate,
        jquery = layui.$;


    jQuery("#userlogin").click(function () {
        location.href = "/userlogo";
    })


    jquery(function() {
        jquery('.imgcode').click(function() {
            //var url = "/captcha/captchaImage?type=math&s=" + Math.random();
            var url = "/captcha/captchaImage?type=math";
            jquery(".imgcode").attr("src", url);
        });
    });




    /*登录点击事件*/
    jquery("#login").click(function () {
        var index = top.layer.msg('正在登录，请稍候', {icon: 16, time: false, shade: 0.8});
        var ucode= jquery("#ucode").val();
        var upassword=jquery("#upassword").val();
        var code = $("input[name='code']").val();

        jquery.ajax({
            url: "/zblogUser/admin"
            , type: "post"
            , data: {
                "ucode":ucode,
                "upassword":upassword,
                "code":code
            }
            , success: function (result) {
                if (result.msg=="/") {
                    layer.close(index)
                    top.layer.msg("用户登录成功！");
                    location.href = "/index";
                }else{
                    layer.close(index)
                    top.layer.msg(result.msg);
                }
            }
        });

    })




});
