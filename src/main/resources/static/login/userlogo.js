layui.use(['form', 'layer', 'jquery', 'layedit', 'laydate'], function () {
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : parent.layer,
        laypage = layui.laypage,
        layedit = layui.layedit,
        laydate = layui.laydate,
        jquery = layui.$;

    jQuery("#login").click(function () {
        location.href = "/admin";
    })
    var token="";
    jQuery("#faong").click(function() {
        var index = top.layer.msg('正在发送验证码，请稍候', {icon: 16, time: false, shade: 0.8});
        var uname=jquery("#uname").val();//用户名
        var ucode= jquery("#ucode").val();//账号
        var upassword=jquery("#upassword").val();//密码

        if(uname == null || uname == ""){
            layer.close(index);
            top.layer.msg("用户名为空");
        }else if(ucode == null || ucode == ""){
            layer.close(index);
            top.layer.msg("账号为空");
        }else if(upassword == null || upassword == ""){
            layer.close(index);
            top.layer.msg("密码为空");
        }else{
            jquery.ajax({
                url: "/zblogUser/registered"
                , type: "post"
                , data: {
                    "uname":uname,
                    "ucode":ucode,
                    "upassword":upassword,
                }
                , success: function (result) {
                    if(result.data!=null){
                        token=result.data;
                        layer.close(index);
                        top.layer.msg("验证码发送成功！");
                    }else if(result.msg!=null){
                        layer.close(index);
                        top.layer.msg(result.msg);
                    }
                }
            });
        }
    });



    /*注册点击事件*/
    jquery("#userlogin").click(function () {
        var index = top.layer.msg('正在注册，请稍候', {icon: 16, time: false, shade: 0.8});
        var uname=jquery("#uname").val();//用户名
        var ucode= jquery("#ucode").val();//账号
        var upassword=jquery("#upassword").val();//密码
        var code= jquery("#code").val();    //获取验证码
        if(uname == null || uname == ""){
            layer.close(index);
            top.layer.msg("用户名为空");
        }else if(ucode == null || ucode == ""){
            layer.close(index);
            top.layer.msg("账号为空");
        }else if(upassword == null || upassword == ""){
            layer.close(index);
            top.layer.msg("密码为空");
        }else if(code == null || code == ""){
            layer.close(index);
            top.layer.msg("验证码空");
        }else{
            jquery.ajax({
                url: "/zblogUser/registeredcode"
                , type: "post"
                , data: {
                    "uname":uname,        //用户名
                    "ucode":ucode,        //账号
                    "upassword":upassword,//密码
                    "code":code,        //验证码
                    "token":token
                }
                , success: function (result) {
                    layer.close(index);
                    top.layer.msg(result.msg);
                    location.href = "/admin";
                }
            });
        }
    })




});
