layui.use(['form', 'layer', 'jquery','upload','laydate'], function () {

    var form = layui.form, jquery = layui.$, layer = layui.layer;



    /**
     * 表单提交
     * */
    form.on("submit(addNew)", function (data) {
        var formData = data.field;//获取form数据
        //弹出loading
        var index = top.layer.msg('数据提交中，请稍候', {icon: 16, time: false, shade: 0.8});
        jquery.ajax({
            url: "/zblogCategory/zblogcategoryadd"
            , contentType: "application/json; charset=utf-8"
            , type: "post"
            , datType: "josn"
            , async: false
            , data: JSON.stringify(formData)
            , success: function (result) {
                layer.close(index)
                top.layer.msg(result.msg);
                parent.location.reload();//刷新数据并关闭添加页面
            }
        });
    });






})