package com.zkc.zblog.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zkc.zblog.entity.ZblogCategory;

import java.util.List;

/**
 * (ZblogCategory)表服务接口
 *
 * @author zkc
 * @since 2019-08-29 21:35:06
 */
public interface ZblogCategoryService extends IService<ZblogCategory> {


    /**
     * 文章修改时 和 添加时 所用的方法  给文章挑选分类
     * */
    public List<ZblogCategory> categoryall();

    /**
    * 分类的分压查询
    * */
    public Page<ZblogCategory> selectAllList(Integer current, Integer size);


}