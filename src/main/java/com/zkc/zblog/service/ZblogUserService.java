package com.zkc.zblog.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zkc.zblog.entity.ZblogUser;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * (ZblogUser)表服务接口
 * @author zkc
 * @since 2019-08-10 10:04:28
 */
public interface ZblogUserService extends IService<ZblogUser> {


    /*登录*/
    public ZblogUser login(String ucode, String upassword) throws Exception;

    /*邮箱发送验证码   进入reids*/
    public  String registered(String ucode) throws Exception;

    /*注册   数据入库*/
    public void registeredcode(String uname,String ucode,String upassword,String code,String token) throws Exception;

    /*查询全部用户*/
    public IPage<ZblogUser> UserALL(Integer pagenum, Integer pagesize) throws Exception;

    /*修改用户前的单个查询*/
    public ZblogUser UserAllUpdate(Long uid) throws Exception;


    /*后台添加用户   相当于注册*/
    public Integer UserAdd(ZblogUser zblogUser);



}