package com.zkc.zblog.service.impl;

import com.zkc.zblog.service.Email;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service("email")
public class EmailImpl implements Email {

    @Autowired
    private JavaMailSender mailSender;

    @Override
    public void Email(String from, String code) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("zkc4918@163.com");//发送人邮箱
        message.setTo(from);//发送给谁    对方邮箱
        message.setSubject("欢迎用户使用：");  //标题
        message.setText("尊敬的用户您好，您的验证码为 ："+code);//内容
        mailSender.send(message);//发送
    }







}
