package com.zkc.zblog.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zkc.zblog.entity.JobAndTrigger;
import com.zkc.zblog.entity.JobForm;
import com.zkc.zblog.mapper.Jobmapper;
import com.zkc.zblog.quartz.JobUtil;
import com.zkc.zblog.service.JobService;

import org.quartz.*;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


@Service("JobService")
public class JobServiceImpl extends ServiceImpl<Jobmapper, JobAndTrigger> implements JobService {


    @Resource
    Scheduler scheduler;
    @Resource
    Jobmapper jobMapper;


    @Override
    public IPage<JobAndTrigger> joblist(Integer pagenum, Integer pagesize) throws Exception {
        QueryWrapper<JobAndTrigger> queryWrapper = new QueryWrapper<>();
        Page<JobAndTrigger> page = new Page<>(pagenum, pagesize);
        IPage<JobAndTrigger> iPage = jobMapper.list(page, queryWrapper);
        return iPage;
    }

    /**
     * 添加并启动定时任务
     */
    @Override
    public void addJob(JobForm form) throws Exception {
        /**
         * 调度器：Scheduler
         *
         * 触发器：Trigger
         *
         * 作业类：JobDetail
         * */

        // 启动调度器
        scheduler.start();
        // 构建Job信息
        JobDetail jobDetail = JobBuilder
                .newJob(JobUtil.getClass(form.getJobClassName()).getClass())
                .withIdentity(form.getJobClassName(), form.getJobGroupName())
                .build();
        // Cron表达式调度构建器(即任务执行的时间)
        CronScheduleBuilder cron = CronScheduleBuilder.cronSchedule(form.getCronExpression());
        //根据Cron表达式构建一个Trigger
        CronTrigger trigger = TriggerBuilder.newTrigger().withIdentity(form.getJobClassName(), form.getJobGroupName()).withSchedule(cron).build();



        trigger.getJobDataMap().put("articlename",form.getArticlename());//文章名字
        trigger.getJobDataMap().put("articlekeyword",form.getArticlekeyword()); //文章关键字
        trigger.getJobDataMap().put("articledescription",form.getArticledescription());//文章描述
        trigger.getJobDataMap().put("artuclecontent",form.getArtuclecontent());//文章内容
        trigger.getJobDataMap().put("artuclethumbnail",form.getArtuclethumbnail());//文章缩略图
        trigger.getJobDataMap().put("artuledata",form.getArtuledata());//文章创建时间
        trigger.getJobDataMap().put("artuleuser",form.getArtuleuser());//文章列表关联的用户表 （zblog_user）外键
        trigger.getJobDataMap().put("artulestatus",form.getArtulestatus()); //1  未审核       2已审核
        trigger.getJobDataMap().put("artulecategory",form.getArtulecategory());//文章类别          关联主键(zblog_category)
        trigger.getJobDataMap().put("artulecarousel",form.getArtulecarousel());//0 不是轮播图推荐文章  1  是轮播图推荐文章


        try {
            scheduler.scheduleJob(jobDetail, trigger);
        } catch (SchedulerException e) {
            throw new Exception("【定时任务】创建失败！");
        }
    }

    @Override
    public void deleteJob(JobForm form) throws Exception {
        scheduler.pauseTrigger(TriggerKey.triggerKey(form.getJobClassName(), form.getJobGroupName()));
        scheduler.unscheduleJob(TriggerKey.triggerKey(form.getJobClassName(), form.getJobGroupName()));
        scheduler.deleteJob(JobKey.jobKey(form.getJobClassName(), form.getJobGroupName()));
    }

}
