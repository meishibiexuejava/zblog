package com.zkc.zblog.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zkc.zblog.entity.Left_nav_one_tree;
import com.zkc.zblog.mapper.ZblogLeftNavmapper;
import com.zkc.zblog.entity.ZblogLeftNav;
import com.zkc.zblog.service.ZblogLeftNavService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (ZblogLeftNav)表服务实现类
 *
 * @author zkc
 * @since 2020-06-16 17:12:29
 */
@Service("zblogLeftNavService")
public class ZblogLeftNavServiceImpl extends ServiceImpl<ZblogLeftNavmapper, ZblogLeftNav> implements ZblogLeftNavService {

     @Resource
     ZblogLeftNavmapper zblogLeftNavmapper;

    /**
     * 后台左侧导航栏管理全部分层级查出
     * */
    @Override
    public List<Left_nav_one_tree> ZblogLeftNavlistTree() {

        List<Left_nav_one_tree> listtree=zblogLeftNavmapper.Tree();


        return listtree;
    }

    /**
     * 后台导航栏分类修改页面
     *
     * */
    @Override
    public ZblogLeftNav UpdateMeun(Integer id) {
        return zblogLeftNavmapper.UpdateMenu(id);
    }

    /**
     * 判断是目录还是分类
     */
    @Override
    public Integer child(Integer codeid) {
        return zblogLeftNavmapper.child(codeid);
    }

    @Override
    public ZblogLeftNav parent(Long code) {
        return zblogLeftNavmapper.parent(code);
    }

    @Override
    public Integer updatemune(ZblogLeftNav zblogLeftNav) {
        return zblogLeftNavmapper.updatemune(zblogLeftNav);
    }


}
