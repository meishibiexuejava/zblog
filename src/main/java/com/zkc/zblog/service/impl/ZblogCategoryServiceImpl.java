package com.zkc.zblog.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zkc.zblog.entity.ZblogArticle;
import com.zkc.zblog.entity.ZblogUser;
import com.zkc.zblog.mapper.ZblogCategorymapper;
import com.zkc.zblog.entity.ZblogCategory;
import com.zkc.zblog.service.ZblogCategoryService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (ZblogCategory)表服务实现类
 *
 * @author zkc
 * @since 2019-08-29 21:35:06
 */
@Service("zblogCategoryService")
public class ZblogCategoryServiceImpl extends ServiceImpl<ZblogCategorymapper, ZblogCategory> implements ZblogCategoryService {

    @Resource
    ZblogCategorymapper zblogCategorymapper;

    @Override
    public List<ZblogCategory> categoryall() {
        QueryWrapper<ZblogCategory> queryWrapper =new QueryWrapper<>();
        queryWrapper.orderByAsc("fid");
        return zblogCategorymapper.selectList(queryWrapper);
    }


    @Override
    public Page<ZblogCategory> selectAllList(Integer current, Integer size) {
        Page<ZblogCategory> page=new Page<>(current,size);
        QueryWrapper<ZblogCategory> queryWrapper=new QueryWrapper();
        queryWrapper.orderByAsc("fid");
        Page<ZblogCategory> Pages=zblogCategorymapper.selectAllList(page,queryWrapper);
        return Pages;
    }
}