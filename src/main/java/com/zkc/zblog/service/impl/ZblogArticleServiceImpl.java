package com.zkc.zblog.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.BeanUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zkc.zblog.entity.ZblogUser;
import com.zkc.zblog.exception.ErrorCode;
import com.zkc.zblog.exception.GotripException;
import com.zkc.zblog.mapper.ZblogArticlemapper;
import com.zkc.zblog.entity.ZblogArticle;
import com.zkc.zblog.service.ZblogArticleService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * (ZblogArticle)表服务实现类
 *
 * @author zkc
 * @since 2019-08-30 20:34:13
 */
@Service("zblogArticleService")
public class ZblogArticleServiceImpl extends ServiceImpl<ZblogArticlemapper, ZblogArticle> implements ZblogArticleService {

    @Resource
    ZblogArticlemapper zblogArticlemapper;

    /**
     * current     当前页
     * size    也大小
     * */
    @Override
    public Page<ZblogUser> selectAllList(Integer current, Integer size) {
        Page<ZblogUser> page= new Page<>(current,size);
        QueryWrapper<ZblogArticle> queryWrapper=new QueryWrapper();
        queryWrapper.orderByAsc("artuledata");
        Page<ZblogUser> Pages=zblogArticlemapper.selectAllList(page,queryWrapper);
        return Pages;
    }

    @Override
    public void addArtic(ZblogArticle zblogArticle) throws Exception{

        if (zblogArticlemapper.insert(zblogArticle)==1){
            throw new GotripException("文章插入成功", ErrorCode.SEARCH_ITRIP_HOTEL_PAGE_FAIL);
        }else{
            throw new GotripException("文章插入失败", ErrorCode.SEARCH_ITRIP_HOTEL_PAGE_DESTINATION_FAIL);
        }
    }


}