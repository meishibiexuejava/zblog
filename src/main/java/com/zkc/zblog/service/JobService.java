package com.zkc.zblog.service;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zkc.zblog.entity.JobAndTrigger;
import com.zkc.zblog.entity.JobForm;
import com.zkc.zblog.entity.ZblogUser;
import org.quartz.SchedulerException;


public interface JobService extends IService<JobAndTrigger> {
    /**
     * 查询定时任务列表
     *
    * */
    IPage<JobAndTrigger> joblist(Integer pagenum, Integer pagesize) throws Exception;
    /**
     * 添加并启动定时任务
     *
     * @param form 表单参数 {@link JobForm}
     * @throws Exception 异常
     */
    void addJob(JobForm form) throws Exception;

    /**
     * 删除定时任务
     * */
    void deleteJob(JobForm form) throws Exception;






}
