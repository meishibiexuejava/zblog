package com.zkc.zblog.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zkc.zblog.entity.ZblogArticle;
import com.zkc.zblog.entity.ZblogUser;

import java.util.List;
import java.util.Map;

/**
 * (ZblogArticle)表服务接口
 *
 * @author zkc
 * @since 2019-08-30 20:34:13
 */
public interface ZblogArticleService extends IService<ZblogArticle> {


    /*全部文章列表*/
    public Page<ZblogUser> selectAllList(Integer current, Integer size);
    /*插入文章*/
    void addArtic(ZblogArticle zblogArticle) throws Exception;





}