package com.zkc.zblog.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zkc.zblog.entity.Left_nav_one_tree;
import com.zkc.zblog.entity.ZblogLeftNav;
import java.util.List;
/**
 * (ZblogLeftNav)表服务接口
 *
 * @author zkc
 * @since 2020-06-16 17:12:29
 */
public interface ZblogLeftNavService extends IService<ZblogLeftNav> {


   /**
    * 后台左侧导航栏管理全部分层级查出
    * */
   List<Left_nav_one_tree> ZblogLeftNavlistTree();

   /**
    * 后台导航栏分类修改页面
    *
    * */
   ZblogLeftNav UpdateMeun(Integer id);


   /**
    * 判断是目录还是分类
    * */
   Integer child(Integer codeid);

   /**
    * 判断是否拥有父级
    */
   ZblogLeftNav parent(Long code);

   /**
    * 判断是否拥有父级
    */
   Integer updatemune(ZblogLeftNav zblogLeftNav);

}
