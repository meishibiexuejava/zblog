package com.zkc.zblog.exception;


public class ErrorCode {


    public final static String AUTH_UNKNOWN = "30000";
    public final static String AUTH_USER_ALREADY_EXISTS = "30001";//用户已存在
    public final static String AUTH_AUTHENTICATION_FAILED = "30002"; //密码错误
    public final static String AUTH_PARAMETER_ERROR = "30003";// 输入的  用户名密码参数错误，为空
    public final static String AUTH_ACTIVATE_FAILED = "30004";// 没有该用户
    public final static String AUTH_REPLACEMENT_FAILED = "30005"; //邮箱格式错误
    public final static String AUTH_TOKEN_INVALID = "30006";  //账户已存在
    public static final String AUTH_ILLEGAL_USERCODE = "30007";   //用户名已存在
    public final static String AUTH_TOKEN_GESHICUOWU="30008";
    public final static String AUTH_TOKEN_BAOHUQI="30009";



    public static final String BIZ_HOTEL_QUERY_HOTCITY_FAIL = "/";             //登录成功
    public static final String BIZ_HOTEL_QUERY_HOTEL_FEATURE_FAIL = "10205";   //验证码不能为空
    public static final String BIZ_HOTEL_QUERY_TRADE_AREA_FAIL = "10204";      //验证码错误
    public static final String BIZ_GET_VIDEO_DESC_HOTELID_FAIL = "100215";     //用户名不存在
    public static final String BIZ_GET_VIDEO_DESC_FAIL = "100214";             //验证码已失效
    public static final String BIZ_QUERY_HOTEL_DETAILS_ID_FAIL = "10210";      //用户名为空
    public static final String BIZ_QUERY_HOTEL_DETAILS_FAIL = "10211";         //用户账号为空
    public static final String BIZ_QUERY_HOTEL_DETAILS_PASSWORD = "10000100";  //密码为空
    public static final String BIZ_QUERY_HOTEL_DETAILS_EMAIL = "10000100";  //toke为空

    public static final String SEARCH_ITRIP_HOTEL_PAGE_DESTINATION_FAIL = "20002"; //文章插入失败
    public static final String SEARCH_ITRIP_HOTEL_PAGE_FAIL = "20001"; //文章插入成功




    //定时任务
    public static final String SEARCH_ITRIP_HOTEL_LIST_BY_HOT_CITY_CITYID_FAIL = "201";
    public static final String SEARCH_ITRIP_HOTEL_LIST_BY_HOT_CITY_FAIL = "500";
    public static final String DING_SHI_REN_WU_QUAN_LEI_MING_JOBCLASSNAME ="定时任务全类名";
    public static final String REN_WU_ZU_MING_JOBGROUPNAME = "任务组名";
    public static final String DELETE_JOB = "定时任务删除失败";







}
