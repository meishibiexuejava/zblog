package com.zkc.zblog.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zkc.zblog.entity.ZblogArticle;
import com.zkc.zblog.entity.ZblogCategory;
import com.zkc.zblog.entity.ZblogUser;
import org.apache.ibatis.annotations.Param;

/**
 * (ZblogCategory)表数据库访问层
 *
 * @author zkc
 * @since 2019-08-29 21:35:06
 */
public interface ZblogCategorymapper extends BaseMapper<ZblogCategory> {
        
        /*查询单个*/
        ZblogCategory selectByPrimarykey(Long fid);

        Page<ZblogCategory> selectAllList(@Param("pg") Page<ZblogCategory> page, @Param("ew") QueryWrapper<ZblogCategory> queryWrapper);
    
    
}