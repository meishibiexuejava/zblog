package com.zkc.zblog.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zkc.zblog.entity.ZblogUser;

/**
 * (ZblogUser)表数据库访问层
 * @author zkc
 * @since 2019-08-10 10:04:28
 */
public interface ZblogUsermapper extends BaseMapper<ZblogUser> {
        /*查询单个*/
        ZblogUser selectByPrimarykey(Long uid);

        /*登录 更具账号经行查询*/
        ZblogUser selectlogin(String ucode);

        /*根据   用户名查询*/
        ZblogUser selectuname(String uname);

        /*注册*/
        int insertSelective(ZblogUser record);
}
