package com.zkc.zblog.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zkc.zblog.entity.ZblogArticle;
import com.zkc.zblog.entity.ZblogUser;
import org.apache.ibatis.annotations.Param;



/**
 * (ZblogArticle)表数据库访问层
 *
 * @author zkc
 * @since 2019-08-30 20:34:13
 */
public interface ZblogArticlemapper extends BaseMapper<ZblogArticle> {
        
        /*查询单个*/
        ZblogArticle selectByPrimarykey(Long wid);

        Page<ZblogUser> selectAllList(@Param("pg") Page<ZblogUser> page,@Param("ew") QueryWrapper<ZblogArticle> queryWrapper);
    
    
}