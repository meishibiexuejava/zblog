package com.zkc.zblog.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zkc.zblog.entity.Left_nav_one_tree;
import com.zkc.zblog.entity.ZblogLeftNav;

import java.util.List;

/**
 * (ZblogLeftNav)表数据库访问层
 *
 * @author zkc
 * @since 2020-06-16 17:12:29
 */
public interface ZblogLeftNavmapper extends BaseMapper<ZblogLeftNav> {

        /**
         * 后台左侧导航栏管理全部分层级查出
         * */
        List<Left_nav_one_tree> Tree();

        /**
         *  导航栏修改页面数据查询
         * */
        ZblogLeftNav UpdateMenu(Integer id);

        /**
         * 判断是目录还是分类
         */
        Integer child(Integer codeid);

        /**
         * 判断是否拥有父级
         */
        ZblogLeftNav parent(Long code);


        Integer updatemune(ZblogLeftNav zblogLeftNav);




}
