package com.zkc.zblog.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.zkc.zblog.entity.JobAndTrigger;
import org.apache.ibatis.annotations.Param;


import java.util.List;



public interface Jobmapper extends BaseMapper<JobAndTrigger> {
    /**
     * 查询定时作业和触发器列表
     *
     * @return 定时作业和触发器列表
     */
    IPage<JobAndTrigger> list(IPage<JobAndTrigger> page, @Param("ew") Wrapper<JobAndTrigger> queryWrapper);
}
