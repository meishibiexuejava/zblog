package com.zkc.zblog.quartz;


public class JobUtil {

    public static HiJob getClass(String classname) throws Exception {
        Class<?> clazz = Class.forName(classname);
        return (HiJob) clazz.newInstance();
    }
}
