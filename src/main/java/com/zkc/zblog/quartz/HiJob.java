package com.zkc.zblog.quartz;


import com.zkc.zblog.entity.ZblogArticle;
import com.zkc.zblog.service.ZblogArticleService;
import org.quartz.*;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;
import java.util.Date;
import org.quartz.JobExecutionException;
import javax.annotation.Resource;


/**
* @PersistJobDataAfterExecution和@DisallowConcurrentExecution注解是不让某个定时任务并发执行，只有等当前任务完成下一个任务才会去执行。
*/

@PersistJobDataAfterExecution
@DisallowConcurrentExecution
@Component
public class HiJob extends QuartzJobBean {



    @Resource
    ZblogArticleService zblogArticleService;



    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {

        JobDataMap data=jobExecutionContext.getTrigger().getJobDataMap();


        ZblogArticle zblogArticle=new ZblogArticle();

        zblogArticle.setArticlename((String) data.get("articlename"));//文章名字
        zblogArticle.setArticlekeyword((String) data.get("articlekeyword"));//文章关键字
        zblogArticle.setArticledescription((String) data.get("articledescription"));//文章描述
        zblogArticle.setArtuclecontent((String) data.get("artuclecontent"));//文章内容
        zblogArticle.setArtuclethumbnail((String) data.get("artuclethumbnail"));//文章缩略图
        zblogArticle.setArtuledata((Date) data.get("artuledata"));//文章创建时间
        zblogArticle.setArtuleuser((Long) data.get("artuleuser"));//文章列表关联的用户表 （zblog_user）外键
        zblogArticle.setArtulestatus((Long) data.get("artulestatus"));//1  未审核       2已审核
        zblogArticle.setArtulecategory((Long) data.get("artulecategory"));//文章类别          关联主键(zblog_category)
        zblogArticle.setArtulecarousel((Long) data.get("artulecarousel"));//0 不是轮播图推荐文章  1  是轮播图推荐文章


        try {
            if(zblogArticleService==null){
                System.out.println("service为空+定时任务未执行！");
            } else{
                zblogArticleService.addArtic(zblogArticle);
                System.out.println("定时任务执行成功");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
