package com.zkc.zblog.controller;

import com.zkc.zblog.entity.ZblogCategory;
import com.zkc.zblog.service.ZblogCategoryService;
import io.swagger.annotations.Api;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;

@Controller
@Api( description = "后台用户页面跳转接口")
@RequestMapping("/nav/category")
public class ZblogCategoryModelController {

    /**
     * 服务对象
     */
    @Resource
    private ZblogCategoryService zblogCategoryService;


    /**
     *修改跳转页面
     *
    * */
    @GetMapping("/categorymodelupdate/{fid}")
    public String updateid(@PathVariable("fid") Long fid, Model model){
        ZblogCategory zblogCategory= zblogCategoryService.getById(fid);
        model.addAttribute("fid",fid);
        model.addAttribute("categoryname",zblogCategory.getCategory());
        return "Backstage/Category/update";
    }


    /**
     * 添加跳转页面
     *
     * */
    @GetMapping("/categorymodeladd")
    public String modeladd(){
        return "Backstage/Category/add";
    }





}
