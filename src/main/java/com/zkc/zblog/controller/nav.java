package com.zkc.zblog.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/nav")
@Api( description = "跳转各个模块的主页面接口")
public class nav {

    /**
     * 后台默认显示页面
     *
     * */
    @ApiOperation(value = "后台默认显示页面" , notes = "进入后台显示的默认界面")
    @RequiresPermissions("nav:default")
    @GetMapping("/default")
    public String defaul(){
        return "Backstage/nva/default";
    }

    /**
     * 后台用户管理页面
     *
     * */
    @ApiOperation(value = "后台用户管理页面" ,notes = "跳转到后台用户管理界面")
    @RequiresPermissions("nav:user")
    @GetMapping("/user")
    public String user(Model model){
        return "Backstage/nva/UserAll";
    }

    /**
     * 后台文章管理界面
     *
     * */
    @ApiOperation(value = "后台文章管理界面" , notes = "跳转到后台文章管理界面")
    @RequiresPermissions("nav:article")
    @GetMapping("/article")
    public String article(){
        return "Backstage/nva/article";
    }

    /**
     * 后台分类管理界面
     *
     * */
    @ApiOperation(value = "后台分类管理界面" , notes = "跳转到后台分类管理界面")
    @RequiresPermissions("nav:Category")
    @GetMapping("/Category")
    public String Category(){
        return "Backstage/nva/Category";
    }

    /**
     * 后台文章定时发布页面
     *
     * */
    @ApiOperation(value = "后台文章定时发布页面", notes = "跳转后台文章定时发布界面")
    @RequiresPermissions("nav:timed_article")
    @GetMapping("timed_article")
    public String Timed_article(Model model){
        return "Backstage/nva/timed_article";
    }


    /**
     * 后台菜单管理
     * */
    @ApiOperation(value = "后台菜单管理", notes = "跳转后台菜单管理界面")
    @RequiresPermissions("nav:menu")
    @GetMapping("menu")
    public String Menu(){
        return  "Backstage/nva/Menu";
    }


    /**
     * 后台个人中心页面
     *
     * */
    @ApiOperation(value = "后台个人中心页面", notes = "跳转到后台个人中心界面")
    @GetMapping("/personal")
    public String personal(){
        return "/Backstage/nva/personal";
    }

    /**
     * 后台用户退出登陆
     *
     * */
    @ApiOperation(value = "后台用户退出登陆", notes = "退出登陆后跳转前台界面")
    @RequiresPermissions("nav:logout")
    @GetMapping("/logout")
    public String logout(){
        Subject subject= SecurityUtils.getSubject();
        subject.logout();
        return "redirect:/content/index";
    }









}
