package com.zkc.zblog.controller;

import com.zkc.zblog.entity.ZblogLeftNav;
import com.zkc.zblog.service.ZblogLeftNavService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import javax.annotation.Resource;


@Controller
@RequestMapping("/nav/nav_menu")
public class ZblogLeftNavModelController {

    @Resource
    ZblogLeftNavService zblogLeftNavService;
    /**
     * 后台导航栏分类修改页面
     *
     * */
    @GetMapping("/menu_update/{id}")
    public String menu_update(@PathVariable("id") Integer id, Model model){
        ZblogLeftNav tree=zblogLeftNavService.UpdateMeun(id);  //根据code 把菜单信息查询出来
        model.addAttribute("meun",tree);

        if (zblogLeftNavService.child(id) != 0){ //判断是否拥有目录  把 code 当作codeid 去查询 有就是目录  没有就是导航
            model.addAttribute("child",tree.getUrl());
        }

        if(tree.getCodeid() == 0){  //更具查询出来的信息 codeid 为 0 说明没有父级菜单
            model.addAttribute("superior","0");
        }else{
            model.addAttribute("superior",zblogLeftNavService.parent(tree.getCodeid()).getNname());
            model.addAttribute("fucode",zblogLeftNavService.parent(tree.getCodeid()).getCode());
        }
         return "Backstage/Menu/Menu_update";
    }


    /**
     *
     * 图标页面
     *
     * */
    @GetMapping("/icon")
    public String icon(){
        return "Backstage/Menu/icon";
    }




}
