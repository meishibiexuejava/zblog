package com.zkc.zblog.controller;

import com.zkc.zblog.entity.ZblogUser;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.SecurityUtils;
import org.springframework.ui.Model;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;


@Controller
@Api( description = "后台登陆、注册、登陆失败、登陆成功、前台显示页面的接口")
public class IndexController {

    /**
     * 后台用户登陆页面
     *
     * */
    @ApiOperation(value = "后台用户登陆页面")
    @GetMapping("/admin")
    public String login(Model model){
        model.addAttribute("title","登录");
        return "login";
    }

    /**
     * 后台用户注册页面
     *
     * */
    @ApiOperation(value = "后台用户注册页面")
    @GetMapping("/userlogo")
    public String userlogo(Model model){
        model.addAttribute("title","注册");
        return "userlogo";
    }

    /**
     * 登陆失败页面
     *
     * */
    @ApiOperation(value = "登陆失败页面")
    @GetMapping("/noauth")
    public String noauth(){
        return "noauth";
    }

    /**
     * 后台登成功显示页面
     *
     * */
    @ApiOperation(value = "后台登成功显示页面")
    @GetMapping("/index")
    public String index(Model model,HttpServletRequest request){
        //System.out.println("Session有效时间"+request.getSession().getMaxInactiveInterval());
        //System.out.println("shiro-Session有效时间"+SecurityUtils.getSubject().getSession().getTimeout());
        ZblogUser zblogUser=(ZblogUser) SecurityUtils.getSubject().getPrincipal();  //获取当前登录用户的个人信息
        model.addAttribute("admin_user",zblogUser);
        return "Backstage/index";
    }

    /**
     * 前台显示页面
     *
     * */
    @ApiOperation(value = "前台显示页面")
    @GetMapping("/")
    public String shouye(){
        return "content/index";
    }


}
