package com.zkc.zblog.controller;

import com.zkc.zblog.entity.ZblogArticle;
import com.zkc.zblog.service.ZblogArticleService;
import com.zkc.zblog.service.ZblogCategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import javax.annotation.Resource;

@Controller
@RequestMapping("/nav/article")
@Api( description = "后台文章跳转页面接口")
public class ZblogArticleModelController {

    @Resource
    ZblogCategoryService zblogCategoryService;
    @Resource
    ZblogArticleService zblogArticleService;

    /**
     * 跳转文章添加页面
     *
     * */
    @ApiOperation(value = "跳转文章添加页面")
    @RequiresPermissions("nav:article:articleadd")
    @GetMapping("/articleadd")
    public String articleadd(Model model){
        model.addAttribute("categoryall",zblogCategoryService.categoryall());
        return "Backstage/article/articleadd";
    }

    /**
     * 跳转文章查看页面
     *
     * */
    @ApiOperation(value = "跳转文章查看页面")
    @GetMapping("/articlelook/{wid}")
    public String articlelook(@ApiParam(value = "文章ID") @PathVariable("wid") Long wid, Model model){
        ZblogArticle zblogArticle=zblogArticleService.getById(wid);
        model.addAttribute("dlook",zblogArticle);
        return "Backstage/article/articlelook";
    }

    /**
     * 跳转文章修改页面
     *
     *
     * */
    @ApiOperation(value = "跳转文章修改页面")
    @GetMapping("/articleupdate/{wid}")
    public String articleupdate(@ApiParam(value = "文章ID") @PathVariable("wid") Long wid, Model model){
        ZblogArticle zblogArticle= zblogArticleService.getById(wid);
        System.out.println(zblogArticle.toString());
        model.addAttribute("categoryall",zblogCategoryService.categoryall());
        model.addAttribute("zblogArticle",zblogArticle);
        return "Backstage/article/articleupdate";
    }

}
