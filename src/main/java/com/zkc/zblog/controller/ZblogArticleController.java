package com.zkc.zblog.controller;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.zkc.zblog.entity.ZblogArticle;
import com.zkc.zblog.entity.ZblogUser;
import com.zkc.zblog.service.ZblogArticleService;
import com.zkc.zblog.utils.Dto;
import com.zkc.zblog.utils.DtoUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.Date;
import java.util.List;


@RestController
@RequestMapping("/zblogArticle")
@Api(description = "后台文章操作接口")
public class ZblogArticleController extends ApiController {

    @Resource
    private ZblogArticleService zblogArticleService;


    /**
     * 后台文章全部查询接口
     *
     * */
    @ApiOperation(value = "后台文章全部查询接口", notes = "查询出所有的文章,使用mybatisplus的分页插件")
    @RequiresPermissions("zblogArticle:zblogArticleList")
    @GetMapping("/zblogArticleList")
    public Dto zblogArticleList(@ApiParam(value = "当前页") @RequestParam("current") Integer current, @ApiParam(value = "页大小") @RequestParam("size") Integer size){
        return DtoUtil.returnDataSuccess(zblogArticleService.selectAllList(current,size));
    }

    /**
     * 后台文章的添加接口
     *
     * */
    @ApiOperation(value = "后台文章的添加接口")
    @RequiresPermissions("zblogArticle:zblogarticleadd")
    @PostMapping("/zblogarticleadd")
    public Dto zblogarticleadd(@RequestBody ZblogArticle zblogArticle){

        ZblogUser zblogUser=(ZblogUser) SecurityUtils.getSubject().getPrincipal();  //获取当前登录用户的个人信息

        Long artuleuser=zblogUser.getYids();     //当前登录作者 与文章的外键
        zblogArticle.setArtuleuser(artuleuser);

        if(StringUtils.isEmpty(zblogArticle.getArtuledata())){//判断文章发布时间是否为空
            zblogArticle.setArtuledata(new Date());
        }
        if(StringUtils.isEmpty(zblogArticle.getArticlekeyword())){//如果关键字为空 直接把标题的内容写入关键字
            zblogArticle.setArticlekeyword(zblogArticle.getArticlename());
        }
        if(StringUtils.isEmpty(zblogArticle.getArticledescription())){//如果关键字描述 直接把标题的内容写入描述
           zblogArticle.setArticledescription(zblogArticle.getArticlename());
        }

        try {
            zblogArticleService.addArtic(zblogArticle);
            return DtoUtil.returnSuccess("文章发布成功");
        } catch (Exception e) {
            String msg = "文章插入失败";
            if (!StringUtils.isEmpty(e.getMessage())) {
                msg = e.getMessage();
            }
            return DtoUtil.returnSuccess(msg);
        }
    }

    /**
     * 后台文章的删除接口
     *
     * */

    @ApiOperation(value = "后台文章的删除接口")
    @DeleteMapping("/deleteArtic/{wid}")
    public Dto deleteArtic(@ApiParam(value = "文章ID") @PathVariable("wid") Long wid){
        if(zblogArticleService.removeById(wid)){
            return DtoUtil.returnSuccess("删除成功");
        }else{
            return DtoUtil.returnSuccess("删除失败");
        }
    }


    @ApiOperation(value = "后台文章的删除接口")
    @RequiresPermissions("zblogArticle:DeleteArticList")
    @PostMapping("/DeleteArticList")
    public Dto DeleteArticList(@ApiParam("批量删除的分类ID") @RequestBody  List<String> widlist){
        System.out.println("批量删除文章");
        if(zblogArticleService.removeByIds(widlist)){
            return DtoUtil.returnSuccess("删除成功");
        }else{
            return DtoUtil.returnSuccess("删除失败");
        }
    }




    /**
     * 后台文章的修改接口
     *
     * */
    @ApiOperation(value = "后台文章的修改接口")
    @RequiresPermissions("zblogArticle:updateArtic")
    @PutMapping("/updateArtic")
    public Dto updateArtic(@RequestBody ZblogArticle zblogArticle){
        if(zblogArticleService.updateById(zblogArticle)){
            return DtoUtil.returnSuccess("修改成功");
        }else{
            return DtoUtil.returnSuccess("修改失败");
        }
    }







}