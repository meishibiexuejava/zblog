package com.zkc.zblog.controller;

import com.zkc.zblog.entity.ZblogUser;
import com.zkc.zblog.md5.DigestUtil;
import com.zkc.zblog.service.ZblogUserService;
import com.zkc.zblog.utils.Dto;
import com.zkc.zblog.utils.DtoUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
@RequestMapping("/nav/user")
@Api( description = "后台用户页面跳转接口")
public class ZblogUserModelController {
    @Resource
    ZblogUserService zblogUserService;
    /**
     * 后台修改用户前的单个查询和跳转到显示页面
     *
     * */
    @ApiOperation(value = "后台修改用户前的单个查询和跳转到显示页面")
    @GetMapping("/UserAllUpdate/{uid}")
    public String UserAllUpdate(@ApiParam(value = "用户ID") @PathVariable("uid") Long uid, Model model) throws Exception {
        ZblogUser zblogUser= zblogUserService.UserAllUpdate(uid);
        model.addAttribute("userdan",zblogUser);
        return "Backstage/UserAll/Update";
    }

    /**
     * 后台用户添加跳转页面
     *
     * */
    @ApiOperation(value = "后台用户添加跳转页面")
    @GetMapping("/UserAlladdye")
    public String UserAlladdye(){
        return  "Backstage/UserAll/add";
    }

}
