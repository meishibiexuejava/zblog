package com.zkc.zblog.controller;

import com.baomidou.mybatisplus.extension.api.ApiController;
import com.zkc.zblog.entity.ZblogLeftNav;
import com.zkc.zblog.service.ZblogLeftNavService;
import com.zkc.zblog.utils.Dto;
import com.zkc.zblog.utils.DtoUtil;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;


/**
 * (ZblogLeftNav)表控制层
 *
 * @author zkc
 * @since 2020-06-16 17:12:29
 */
@RestController
@RequestMapping("/zblogLeftNav")
public class ZblogLeftNavController extends ApiController {
    /**
     * 服务对象
     */
    @Resource
    private ZblogLeftNavService zblogLeftNavService;


    /**
     * 后台左侧导航栏全部分层级查出
     * 后台左侧导航栏管理全部分层级查出
     * @return 所有数据
     */
    @GetMapping("/tree")
    public Dto selectAllTree() {
        return DtoUtil.returnDataSuccess(zblogLeftNavService.ZblogLeftNavlistTree());
    }


    /**
     *  修改数据的保存操作
     * */
    @PutMapping("/updatemenu")
    public Dto UpdateMenu(@RequestBody ZblogLeftNav zblogLeftNav){

        if(zblogLeftNavService.updatemune(zblogLeftNav)==1){
            return DtoUtil.returnSuccess("修改成功");
        }else {
            return DtoUtil.returnSuccess("修改失败");
        }

    }








}
