package com.zkc.zblog.controller;


import com.zkc.zblog.imagesutils.FileUploadUtils;
import com.zkc.zblog.imagesutils.Global;
import com.zkc.zblog.utils.Dto;
import com.zkc.zblog.utils.DtoUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;


@Controller
@Api(description = "文件上传")
public class UploadImage {
    @ApiOperation(value = "图片上传接口")
    @PostMapping("/upload")
    @ResponseBody
    public Dto updateAvatar(@RequestParam("imgFile") MultipartFile imgFile) throws IOException {
        /*判断图片不为空*/
        if (!imgFile.isEmpty()) {
            File fileDir = Global.getImgDirFile();  //返回生成的图片文件假根目录
            String avatar = FileUploadUtils.upload(fileDir, imgFile);
            return DtoUtil.returnDataSuccess(avatar);
        }
        return DtoUtil.returnDataSuccess("上传失败！");
    }
}
