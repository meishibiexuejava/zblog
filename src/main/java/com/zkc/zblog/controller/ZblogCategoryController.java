package com.zkc.zblog.controller;



import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zkc.zblog.entity.ZblogCategory;
import com.zkc.zblog.service.ZblogCategoryService;
import com.zkc.zblog.utils.Dto;
import com.zkc.zblog.utils.DtoUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.xml.crypto.Data;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * (ZblogCategory)表控制层
 * @author zkc
 *
 */
@RestController
@RequestMapping("zblogCategory")
@Api(description = "后台文章分类的操作接口")
public class ZblogCategoryController extends ApiController {
    /**
     * 服务对象
     */
    @Resource
    private ZblogCategoryService zblogCategoryService;

    /**
     * 分页查询所有数据
     * @return 所有数据
     */
    @ApiOperation(value = "文章分类全部查询接口", notes = "查询出所有的分类,使用mybatisplus的分页插件")
    @RequiresPermissions("zblogCategory:zblogCategoryList")
    @GetMapping("/zblogCategoryList")
    public Dto selectAll(@ApiParam(value = "当前页") @RequestParam("current") Integer current, @ApiParam(value = "页大小") @RequestParam("size") Integer size) {
        return DtoUtil.returnDataSuccess(zblogCategoryService.selectAllList(current,size));
    }

    /**
     * 文章分类添加接接口
     */
    @ApiOperation(value = "文章分类添加接口",notes = "添加文章分类")
    @RequiresPermissions("zblogCategory:zblogcategoryadd")
    @PostMapping("/zblogcategoryadd")
    public Dto zblogcategoryadd(@RequestBody ZblogCategory zblogCategory){
        if(zblogCategoryService.save(zblogCategory)){
            return DtoUtil.returnSuccess("添加成功");
        }else{
            return DtoUtil.returnSuccess("添加失败");
        }
    }

    /**
     * 文章分类删除接口
     *
     *
     * */
    @ApiOperation(value = "文章分类删除接口" ,notes = "删除文章分类接口")
    @RequiresPermissions("zblogCategory:zblogcategorydelete")
    @DeleteMapping("/zblogcategorydelete/{fid}")
    public Dto zblogcategorydelete(@ApiParam("文章分类ID")  @PathVariable("fid") Long fid){
        if(zblogCategoryService.removeById(fid)){
            return DtoUtil.returnSuccess("删除成功");
        }else{
            return DtoUtil.returnSuccess("删除失败");
        }
    }

    /**
     * 文章分类批量删除接口
     * */
    @ApiOperation(value = "文章分类批量删除接口" ,notes = "批量删除文章分类接口")
    @RequiresPermissions("zblogCategory:zblogcategorydeletelist")
    @PostMapping("/zblogcategorydeletelist")
    public Dto zblogcategorydeletelist(@ApiParam("批量删除的分类ID") @RequestBody List<String> arr){


        if(zblogCategoryService.removeByIds(arr)){
            return DtoUtil.returnSuccess("删除成功");
        }else{
            return DtoUtil.returnSuccess("删除失败");
        }


    }



    /**
     * 文章分类修改接口
     *
     * */
    @ApiOperation(value = "文章分类修改接口",notes = "修改文章分类")
    @RequiresPermissions("zblogCategory:zblogcategoryupdate")
    @PutMapping("/zblogcategoryupdate")
    public Dto zblogcategoryupdate(@RequestBody ZblogCategory zblogCategory){
        if(zblogCategoryService.updateById(zblogCategory)){
            return DtoUtil.returnSuccess("修改成功");
        }else{
            return DtoUtil.returnSuccess("修改失败");
        }
    }





















}
