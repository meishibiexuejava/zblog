package com.zkc.zblog.controller;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.UUID;
import com.zkc.zblog.exception.KeyCode;
import com.zkc.zblog.redis.Token;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import com.google.code.kaptcha.Producer;
import org.springframework.web.servlet.ModelAndView;
import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/*
验证码
*/
@Controller
@RequestMapping("/captcha")
@Api( description = "验证码")
public class Vcode {

    @Resource(name = "captchaProducer")
    private Producer captchaProducer;
    @Resource(name = "captchaProducerMath")
    private Producer captchaProducerMath;
    @Resource
    Token token;


    /**
     * 验证码生成接口
     *
     *
     * */
    @ApiOperation(value = "验证码生成接口")
    @GetMapping(value = "/captchaImage")
    public ModelAndView getKaptchaImage(HttpServletRequest request, HttpServletResponse response) {
        ServletOutputStream out = null;
        try {
            //HttpSession session = request.getSession();
            response.setDateHeader("Expires", 0);
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
            response.setContentType("image/jpeg");
            String type = request.getParameter("type");
            String capStr = null;
            String code = null;    //验证码
            BufferedImage bi = null;
            if ("math".equals(type)) {
                String capText = captchaProducerMath.createText();
                capStr = capText.substring(0, capText.lastIndexOf("@"));
                code = capText.substring(capText.lastIndexOf("@") + 1);
                bi = captchaProducerMath.createImage(capStr);;
            } else if ("char".equals(type)) {
                capStr = code = captchaProducer.createText();
                bi = captchaProducer.createImage(capStr);
            }
            /**
             * 对验证码的key进行了拼接
             * */
            StringBuffer sb=new StringBuffer("code:");
            String logo_key = UUID.randomUUID().toString().replaceAll("-", "");//使用uuid生成验证码的key
            sb.append(logo_key);

            Cookie cookie=new Cookie(KeyCode.VERIFY_ID,sb.toString());  //将验证码的key存入cookie中
            cookie.setMaxAge(2*60);   //设置cookie失效时间
            cookie.setPath("/");     //cookie有效路径是网站根目录
            response.addCookie(cookie);  //向客户端写入
            token.logocode(sb.toString(),code);   //将验证码的key 和 value 存入redis中

            out = response.getOutputStream();
            ImageIO.write(bi, "jpg", out);
            out.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

}
