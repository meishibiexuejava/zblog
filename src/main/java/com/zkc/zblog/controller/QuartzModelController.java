package com.zkc.zblog.controller;

import com.zkc.zblog.service.ZblogCategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;

@Controller
@RequestMapping("/nav/quartz")
@Api(value = "后台定时任务功能的跳转页面接口")
public class QuartzModelController {

    @Resource
    ZblogCategoryService zblogCategoryService;

    /**
     * 跳转到后台定时任务添加页面
     *
     * */
    @GetMapping("/addjob")
    @ApiOperation(value = "跳转到后台定时任务添加页面", notes = "定时发布文章的添加界面")
    public String addjob(Model model){
        model.addAttribute("categoryall",zblogCategoryService.categoryall());
        return "Backstage/timedarticle/addjob";
    }






}
