package com.zkc.zblog.redis;

import com.zkc.zblog.entity.ZblogUser;


public interface Token {


    /**
     * 生成邮箱验证码key
     *
     *
     * */
    public String stringtoken(String code);

    /**
     * 将邮箱验证码和key 存入redis 中
     *
     * */
    public void savecode(String token,String code);
    /**
     *将登录验证码和key 存入redis 中
     *
     * */
    public void logocode(String token,String code);

    /**
     * 生成token
     * @param  user 用户信息
     * @return  token 令牌
     * token:[MOBILE|PC]-userCode(md5)-userId-yyyyMMddHHmmss-浏览器的标识
     * @return
     */
    public String generateToken(ZblogUser user);
    /**
     *保存token
     *
     *
     * @param token    token令牌
     * @param user    用户信息
     */
    public void saveToken(String token, ZblogUser user);






}
