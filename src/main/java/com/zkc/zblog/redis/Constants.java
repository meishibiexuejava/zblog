package com.zkc.zblog.redis;

/**
 * Created by sam on 2018/2/9.
 */
public class Constants {
    //默认显示第一页
    public static final int DEFAULT_PAGE_NO = 1;
    //默认一页10条
    public static final int DEFAULT_PAGE_SIZE = 10;

    public static class RedisExpire {
        public static final long DEFAULT_EXPIRE = 60;//60s 有慢sql，超时时间设置长一点
        public final static int SESSION_TIMEOUT = 2 * 60 * 60;//默认2h  注册用户信息
        public final static int SESSION_TIMEOUTS = 3*60;//   验证码
    }

    public static class RedisKeyPrefix{
        //邮箱验证码
        public static final String ACTIVATION_MOBILE_PREFIX = "activation_mobile:";
    }

    public static class LogoReidsKeyPrefix{
        //登录验证码
        public static final String ACTIVATION_MOBILE_PREFIX = "activation_mobile:";
    }
    public static class UserActivateStatus{
        //启用
        public static final  int USER_ACTIVATE_ENABLE=1;
        //禁用
        public static final  int USER_ACTIVATE_DISABLE=0;
    }


}
