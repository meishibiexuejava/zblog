package com.zkc.zblog.config;


import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;
@EnableTransactionManagement
@Configuration
@MapperScan("com.zkc.zblog.mapper")
public class MybatisPlusConfig {
    /**
     *分页插件
     */
    @Bean
    public PaginationInterceptor paginationInterceptor(){
        PaginationInterceptor Paginainterceptor = new PaginationInterceptor();
        /**
         * 分页 一页显示25条
         * */
        Paginainterceptor.setLimit(25);
        return Paginainterceptor;
    }
}


















