package com.zkc.zblog.imagesutils;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.File;

/**
 * @author 24188
 */
public class Global {
    private static final Logger log = LoggerFactory.getLogger(Global.class);
    public static File getImgDirFile() {
        // 构建上传文件的存放 "文件夹" 路径
        String fileDirPath = new String("/zblogimg");//获取配置文件中的路径
        File fileDir = new File(fileDirPath);
        if (!fileDir.exists()) {   //判断文件家是否存在,如果在就不创建新的文件夹
            // 递归生成文件夹
            fileDir.mkdirs();
        }
        return fileDir;//返回生成的图片文件假根目录
    }




}
