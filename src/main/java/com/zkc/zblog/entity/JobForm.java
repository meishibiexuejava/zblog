package com.zkc.zblog.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import java.util.Date;

/**
 * <p>
 * 定时任务详情
 * </p>
 *
 * @package: com.xkcoding.task.quartz.entity.form
 * @description: 定时任务详情
 * @author: yangkai.shen
 * @date: Created in 2018-11-26 13:42
 * @copyright: Copyright (c) 2018
 * @version: V1.0
 * @modified: yangkai.shen
 */
@Data
@ToString
public class JobForm extends Model<JobForm> {

    //定时任务全类名（就是指定定时任务文件的位置）
    private String jobClassName;
    //任务组名（定时任务的名字）
    private String jobGroupName;
    //定时任务cron表达式
    private String cronExpression;


    //文章名字
    private String articlename;
    //文章关键字
    private String articlekeyword;
    //文章描述
    private String articledescription;
    //文章内容
    private String artuclecontent;
    //文章缩略图
    private String artuclethumbnail;
    //文章创建时间
    private Date artuledata;
    //文章列表关联的用户表 （zblog_user）外键
    private Long artuleuser;
    //1  未审核       2已审核
    private Long artulestatus;
    //文章类别          关联主键(zblog_category)
    private Long artulecategory;
    //0 不是轮播图推荐文章  1  是轮播图推荐文章
    private Long artulecarousel;








}
