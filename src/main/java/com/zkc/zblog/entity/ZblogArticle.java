package com.zkc.zblog.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;

import com.sun.org.apache.xpath.internal.objects.XBoolean;
import lombok.Data;
import lombok.ToString;
/**
 * (ZblogArticle)表实体类
 *
 * @author zkc
 * @since 2019-08-30 20:34:13
 */
@Data
@ToString
public class ZblogArticle extends Model<ZblogArticle> {
    //文章表主键
    //主键生成策略，value属性可选 ，但是当属性名不一样事必填
    /**
     *type:配置主键生成策略  Idtype.AUTo表示自增
     *AUTO     数据库自增
     *INPUT    自动输入
     *ID_WORKER    分布式全局唯一id  长整型类型
     *UUID         32位uuid字符串
     *NONE         无状态
     *ID_WORKER_STR     分布式全局唯一id  字符串类型
     */
    @TableId(value = "wid",type = IdType.AUTO)
    private Long wid;
    //文章名字
    private String articlename;
    //文章关键字
    private String articlekeyword;
    //文章描述
    private String articledescription;
    //文章内容
    private String artuclecontent;
    //文章缩略图
    private String artuclethumbnail;
    //文章创建时间
    private Date artuledata;
    //文章列表关联的用户表 （zblog_user）外键
    private Long artuleuser;
    //1  未审核       2已审核
    private Long artulestatus;
    //文章类别          关联主键(zblog_category)
    private Long artulecategory;
    //0 不是轮播图推荐文章  1  是轮播图推荐文章
    private Long artulecarousel;

    /*附加属性*/
    @TableField(exist = false) //表示不是数据库字段
    private ZblogUser zblogUser;
    @TableField(exist = false)//表示不是数据库字段
    private ZblogCategory zblogCategory;


    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.wid;
    }







    
}