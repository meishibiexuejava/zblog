package com.zkc.zblog.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import lombok.Data;
import lombok.ToString;

/**
 * (ZblogLeftNav)表实体类
 *
 * @author zkc
 * @since 2020-06-16 17:12:29
 */
@Data
@ToString
public class ZblogLeftNav extends Model<ZblogLeftNav> {

    //自增id
    @TableId(value = "nid",type = IdType.AUTO)
    private Long nid;
    //编号  （随机编号测试数据因是手动添加所以就和id一样，后期是随机的UUID）
    private Long code;
    //父编号
    private Long codeid;
    //菜单名称
    private String nname;
    //图标
    private String icon;
    //菜单等级
    private Long level;
    //跳转地址 / 目录
    private String url;
    //排序字段（越小越靠前）
    private Long sort;


    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.nid;
    }

}
