package com.zkc.zblog.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import java.util.Date;

import lombok.Data;
import lombok.ToString;
/**
 * (ZblogUser)表实体类
 *
 * @author zkc
 * @since 2019-08-10 10:04:28
 */
@Data
@ToString
public class ZblogUser extends Model<ZblogUser> implements Serializable{

    //用户id
    //主键生成策略，value属性可选 ，但是当属性名不一样事必填
    /*
    *type:配置主键生成策略  Idtype.AUTo表示自增
    *AUTO     数据库自增
    *INPUT    自动输入
    *ID_WORKER    分布式全局唯一id  长整型类型
    *UUID         32位uuid字符串
    *NONE         无状态
    *ID_WORKER_STR     分布式全局唯一id  字符串类型
    */
    @TableId(value = "uid",type = IdType.AUTO)
    private Long uid;
    //用户名
    private String uname;
    //用户账号
    private String ucode;
    //密码
    private String upassword;
    //性别 1保密  2男  3女 0空
    private String usex;
    //头像
    private String uavatar;
    //个性签名
    private String usignature;
    //激活状态    1未激活   2已激活
    private Long ustatus;
    //用户分类    1管理员   2普通用户
    private Long uclassification;
    //用户外键
    private Long yids;
    //邮箱
    private String email;
    //用户创建时间
    private Date userdata;


    public ZblogUser(String uname, String ucode, String upassword) {
        this.uname = uname;
        this.ucode = ucode;
        this.upassword = upassword;
    }

    public ZblogUser(String ucode) {
        this.ucode = ucode;
    }

    public ZblogUser() {

    }

    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.uid;
    }
    
}