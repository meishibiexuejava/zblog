package com.zkc.zblog.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
public class Left_nav_two_tree extends Model<Left_nav_two_tree> {

    //编号
    private Long id;
    //菜单栏名字
    private String title;
    //图标
    private String two_icon;
    //菜单等级
    private Long two_level;
    //跳转地址
    private String href;


    @TableField(exist = false)
    private List<Left_nav_three_tree> children;
}
