package com.zkc.zblog.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Left_nav_three_tree extends Model<Left_nav_three_tree> {
    //编号
    private Long id;
    //菜单栏名字
    private String title;
    //菜单等级
    private Long three_level;
    //图标
    private String three_icon;
    //跳转地址
    private String href;

}
