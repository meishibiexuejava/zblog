package com.zkc.zblog.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import lombok.Data;
import lombok.ToString;
/**
 * (ZblogCategory)表实体类
 *
 * @author zkc
 * @since 2019-08-29 21:35:06
 */
@Data
@ToString
public class ZblogCategory extends Model<ZblogCategory> {
    //文章分类id
    //主键生成策略，value属性可选 ，但是当属性名不一样事必填
    /*
     *type:配置主键生成策略  Idtype.AUTo表示自增
     *AUTO     数据库自增
     *INPUT    自动输入
     *ID_WORKER    分布式全局唯一id  长整型类型
     *UUID         32位uuid字符串
     *NONE         无状态
     *ID_WORKER_STR     分布式全局唯一id  字符串类型
     */
    @TableId(value = "fid",type = IdType.AUTO)
    private Long fid;
    //文章类别
    private String category;




    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.fid;
    }
    
}